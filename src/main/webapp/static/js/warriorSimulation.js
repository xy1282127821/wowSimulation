var time;
var endTime;
var startTime;
var lastMainHandAttackTime=0;
var lastOffHandAttackTime=0;
var lastBloodthirstyTime=0;
var lastAngerManagementTime=0;
var lastCycloneTime=0;
var lastExecuteTime=0;
var lastBloodrage=0;
var lastDeathWishTime=0;

var mainHandSpeed;
var mainHandLowDamage;
var mainHandHighDamage;
var mainHandDef; //主手偏斜伤害率；
var mainHandMissRate;
var mainHandDodgeRate;
var mainHandMissSection;
var mainHandDodgeSection;
var mainHandDefSection;
var mainCrITMissSection;

var offHandSpeed;
var offHandLowDamage;
var offHandHighDamage;
var offHandDef; //主手偏斜伤害率；
var offHandMissRate;
var offHandDodgeRate;
var offHandMissSection;
var offHandDodgeSection;
var offHandDefSection;
var offCrITMissSection;


var skillMissRate;
var skllCrit;


var intervalset;
var injuryFree;
var flurryTime=0;
var data;

var anger=0;
var isHeroicStrike = false;
var isBloodrage = false;
var deathWish=1;

var isHandOfJustice=false;
var isCyclone=true;
var isRash=true;
var isExecute = true;

var duration = 120;
var deathWishTimes = 1;
function startSimulation(){
	if(attr){
		time=0;
		initSimulation()
		initDatagrid();
		endTime =  new Date().getTime();
		intervalset=setInterval(warriorSimulation,10);
	}else{
		alert('人物属性没加载成功,请稍等');
	}

	
}
function initSimulation(){
	if(attr.special.indexOf('11815')>=0){
		isHandOfJustice = true;
	}else{
		isHandOfJustice = false;
	}
	
	injuryFree = getDr(armor);
//	mainHandSpeed = attr.mainHandSpeed*1000;
	mainHandSpeed=parseFloat(1/(1.3/attr.mainHandSpeed))*1000;
	offHandSpeed=parseFloat(1/(1.3/attr.offHandSpeed))*1000;
	
	
	mainHandLowDamage = parseInt(attr.mainHandDamage.split('-')[0]);
	mainHandHighDamage = parseInt(attr.mainHandDamage.split('-')[1]);
	mainHandDef = getDef(attr.mainHandSkill);
	mainHandDodgeRate = getDodgeRate(attr.mainHandSkill);
	if(offHandSpeed==0){
		mainHandMissRate = getSkillMissRate(attr.hitRate,attr.mainHandSkill);
	}else{
		mainHandMissRate=  getMissRate(attr.hitRate,attr.mainHandSkill);  
	}
	
	mainHandMissSection = parseFloat(mainHandMissRate/100);
	mainHandDodgeSection = parseFloat(mainHandMissRate/100+mainHandDodgeRate/100);
	mainHandDefSection = parseFloat(mainHandMissRate/100+mainHandDodgeRate/100+0.4);
	mainHandCritSection = parseFloat(mainHandMissRate/100+mainHandDodgeRate/100+0.4+attr.mainHandCrit/100);
	
	/*console.log("主手伤害上限："+mainHandHighDamage)
	console.log("主手伤害下限："+mainHandLowDamage)
	console.log("主手偏斜伤害："+mainHandDef)
    console.log("主手被躲闪率："+mainHandDodgeRate)
    console.log("主手MISS率："+mainHandMissRate)
    console.log("主手MISS区间："+mainHandMissSection)
    console.log("主手被躲闪区间："+mainHandDodgeSection)
    console.log("主手偏斜区间："+mainHandDefSection)
    console.log("主手暴击区间："+mainHandCritSection)
*/    
	  console.log("主手MISS区间："+mainHandMissSection)
	    console.log("主手被躲闪区间："+mainHandDodgeSection)
	    console.log("主手偏斜区间："+mainHandDefSection)
	    console.log("主手暴击区间："+mainHandCritSection)
    if(offHandSpeed!=0){
	   	offHandLowDamage = parseInt(attr.offHandDamage.split('-')[0]);
		offHandHighDamage = parseInt(attr.offHandDamage.split('-')[1]);
		offHandDef = getDef(attr.offHandSkill);
		offHandDodgeRate = getDodgeRate(attr.offHandSkill);
		offHandMissRate = getMissRate(attr.hitRate,attr.offHandSkill);
		
		offHandMissSection = parseFloat(offHandMissRate/100);
		offHandDodgeSection = parseFloat(offHandMissRate/100+offHandDodgeRate/100);
		offHandDefSection = parseFloat(offHandMissRate/100+offHandDodgeRate/100+0.4);
		offHandCritSection = parseFloat(offHandMissRate/100+offHandDodgeRate/100+0.4+attr.offHandCrit/100);
    }
	
/*	console.log("副手偏斜伤害："+offHandDef)
    console.log("副手被躲闪率："+offHandDodgeRate)
    console.log("副手MISS率："+offHandMissRate)
    console.log("副手MISS区间："+offHandMissSection)
    console.log("副手被躲闪区间："+offHandDodgeSection)
    console.log("副手偏斜区间："+offHandDefSection)
    console.log("副手暴击区间："+offHandCritSection)*/
    
    skillMissRate =getSkillMissRate(attr.hiteRate,attr.mainHandSkill);
	skllCrit = attr.mainHandCrit;
	
	deathWish=1;
	isCyclone = $('#isCyclone').is(':checked');
	isRash = $('#isRash').is(':checked');
	isExecute = $('#isExecute').is(':checked');
	
	duration = $('#duration').combobox('getValue');
	console.log("战斗时长："+duration);
	console.log("斩杀阶段："+(duration-duration*0.2*0.8));
	
	
	if(duration%180>=20){
		deathWishTimes = parseInt(duration/180)+1;
	}else{
		deathWishTimes = parseInt(duration/180);
	}
	console.log("死亡之愿次数："+deathWishTimes);
}
function stopSimulation(){
	$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'停止模拟...'+'</p>')
	clearInterval(intervalset);
	scrollBottom();
	
	var total=0;
	for(var i=0;i<data.length;i++){
		total+=data[i].total;
	}
	
	for( i=0;i<data.length;i++){
		data[i].percent = parseFloat(data[i].total/total*100).toFixed(2)+"%";
	}
	$('#static').datagrid({
		data:data
	})
	$("#static").datagrid('reload');
}
function warriorSimulation(){
    startTime = new Date()
    if(startTime-endTime>=duration*1000){
    	stopSimulation();
    	return;
    }
    //死亡之愿结束
    if(startTime.getTime()-lastDeathWishTime>=30*1000){
    	if(deathWish==1.3){
    		deathWish= 1.0;
    		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'<font color="red">死亡之愿效果结束</font>'+'</p>')
        	scrollBottom();
    	}
    }
    //死亡之愿
    if(deathWishTimes>1&&(startTime.getTime()-lastDeathWishTime)>180*1000){
    	if(deathWish==1){
    		deathWishTimes--;
    		deathWish= 1.3;
    		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'开启了<font color="red">死亡之愿</font>'+'</p>')
        	scrollBottom();
    		lastDeathWishTime=startTime.getTime();
    	}
    }
    if(deathWishTimes==1&&(startTime.getTime()-lastDeathWishTime)>180*1000&&(startTime-endTime)>= (duration-30)*1000){
    	if(deathWish==1){
    		deathWish= 1.3;
        	$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'开启了<font color="red">死亡之愿</font>'+'</p>')
        	scrollBottom();
        	lastDeathWishTime=startTime.getTime();
    	}
    }
    //愤怒掌控
    if(startTime.getTime()-lastAngerManagementTime>3*1000){
    	$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'因为愤怒掌控,产生一点怒气'+'</p>')
		showAnger(1);
    	lastAngerManagementTime=startTime.getTime();
    	scrollBottom();
    }
    //血性狂暴
    if(startTime-endTime<=10*1000&&startTime.getTime()-lastBloodrage>=60*1000){
    	$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'使用血性狂暴,产生10点怒气'+'</p>')
    	showAnger(20);
    	lastBloodrage=startTime.getTime();
    	scrollBottom();
    }
    //主手攻击
    if(startTime.getTime()-lastMainHandAttackTime>=mainHandSpeed||lastMainHandAttackTime==0){
    	if(isHeroicStrike&&anger>=12){
    		getHeroicStrike();
    	}else{
    		isHeroicStrike= false;
    		getMainHandDamage();
    	}
    	lastMainHandAttackTime = startTime.getTime();
    }
  //副手攻击
    if(offHandSpeed!=0&&(startTime.getTime()-lastOffHandAttackTime>=offHandSpeed||lastOffHandAttackTime==0)){
    	if(isHeroicStrike){
    		offHandMissSection = parseFloat(skillMissRate/100);
    		offHandDodgeSection = parseFloat(skillMissRate/100+offHandDodgeRate/100);
    		offHandDefSection = parseFloat(skillMissRate/100+offHandDodgeRate/100+0.4);
    		offHandCritSection = parseFloat(skillMissRate/100+offHandDodgeRate/100+0.4+attr.offHandCrit/100);
    		getOffHandDamage();
    	}else{
    		
    		offHandMissSection = parseFloat(offHandMissRate/100);
    		offHandDodgeSection = parseFloat(offHandMissRate/100+offHandDodgeRate/100);
    		offHandDefSection = parseFloat(offHandMissRate/100+offHandDodgeRate/100+0.4);
    		offHandCritSection = parseFloat(offHandMissRate/100+offHandDodgeRate/100+0.4+attr.offHandCrit/100);
    	 	getOffHandDamage();
    	}

    	lastOffHandAttackTime = startTime.getTime();
    }
    //常规循环
    if(startTime-endTime<=(duration-duration*0.2*0.8)*1000||isExecute==false){
	    if(anger>30&&(startTime.getTime()-lastBloodthirstyTime>=6*1000)){
	    	getBloodthirsty();
	    	lastBloodthirstyTime = startTime.getTime();
	    }
	    if(isCyclone&&(anger>25&&(startTime.getTime()-lastBloodthirstyTime<=3*1000)||anger>40)&&(startTime.getTime()-lastCycloneTime>=10*1000)){
	    	getCyclone();
	    	lastCycloneTime = startTime.getTime();
	    }
	    if(anger>50){
	    	isHeroicStrike=true;
	    }
    }else{//斩杀阶段
    	if(anger>10&&startTime.getTime()-lastExecuteTime>=1.5*1000){
    		getExecute();
    		lastExecuteTime= startTime.getTime();
    	}
    }
    if(startTime-endTime>=(duration-15)*1000){
    	if(mainHandCritSection!=1&&isRash){
	    	$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'<font color="red">使用鲁莽</font>'+'</p>')
	    	mainHandCritSection=1;
	    	skllCrit=100;
	    	scrollBottom();
    	}
    }
    
}
function initDatagrid(){
	 data=[{
		'name':'主手攻击',
		'times':0,
		'total':0,
		'crit':0,
		'critTimes':0,
		'miss':0,
		'missTimes':0,
		'dodge':0,
		'dodgeTimes':0
		
	},{
		'name':'副手攻击',
		'times':0,
		'total':0,
		'crit':0,
		'critTimes':0,
		'miss':0,
		'missTimes':0,
		'dodge':0,
		'dodgeTimes':0,
	},{
		'name':'英勇打击',
		'times':0,
		'total':0,
		'crit':0,
		'critTimes':0,
		'miss':0,
		'missTimes':0,
		'dodge':0,
		'dodgeTimes':0
		
	},{
		'name':'嗜血',
		'times':0,
		'total':0,
		'crit':0,
		'critTimes':0,
		'miss':0,
		'missTimes':0,
		'dodge':0,
		'dodgeTimes':0
		
	},{
		'name':'旋风斩',
		'times':0,
		'total':0,
		'crit':0,
		'critTimes':0,
		'miss':0,
		'missTimes':0,
		'dodge':0,
		'dodgeTimes':0
	},{
		'name':'斩杀',
		'times':0,
		'total':0,
		'crit':0,
		'critTimes':0,
		'miss':0,
		'missTimes':0,
		'dodge':0,
		'dodgeTimes':0
	}
	];
	 if(isHandOfJustice){
		 data.push({
				'name':'正义之手',
				'times':0,
				'total':0,
				'crit':0,
				'critTimes':0,
				'miss':0,
				'missTimes':0,
				'dodge':0,
				'dodgeTimes':0
			})
		 
	 }
	
	
	$('#static').datagrid({
		data:data
	})
	$("#static").datagrid('reload');
}

function reload(){
	var total=0;
	for(var i=0;i<data.length;i++){
		total+=data[i].total;
	}
	for( i=0;i<data.length;i++){
		data[i].percent = parseFloat(data[i].total/total*100).toFixed(2)+"%";
	}
	$('#total').text("总伤害:"+total);
	$('#dps').text("秒伤:"+parseFloat(total/((startTime-endTime)/1000)).toFixed(2));
	$('#static').datagrid({
		data:data
	})
	$("#static").datagrid('reload');
}
function showAnger(a){
	anger+=a;
	if(anger>100){
		anger=100;
	}else if(anger<0){
		console.log(a);
	}
	$('#anger').text("怒气:"+anger);
}

function getDr(ac){
	 if(ac<0){
		 return 0;
	 }
	 var dr=ac/(ac+85*60+400) ;
	 return dr;
}
//斩杀
function getExecute(){
	var random = Math.random();
	var r=0;
	if(random<skillMissRate/100){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次斩杀,miss了'+'</p>');
		data[5].missTimes+=1;
	}
	random = Math.random();
	if(random<mainHandDodgeRate/100){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次斩杀,被躲闪了'+'</p>')
		data[5].dodgeTimes+=1;
	}
	random = Math.random();
	if(random<skllCrit/100){
		r = (600+(anger-10)*15)*2*1.2*(1-injuryFree)*deathWish;
		flurryTime=3;
		showFlurry();
		data[5].critTimes+=1;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次斩杀,暴击造成'+parseInt(r)+'点伤害</p>')
		unbridledWrath();
		effect();
	}else{
		r = (600+(anger-10)*15)*deathWish;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次斩杀,命中造成'+parseInt(r)+'点伤害</p>')
		unbridledWrath();
		effect();
	}
	showAnger(-anger);
	data[5].total+=Math.round(r);
	data[5].times+=1;
	data[5].crit= parseFloat((data[5].critTimes/data[5].times)*100).toFixed(2)+"%";
	data[5].miss= parseFloat((data[5].missTimes/data[5].times)*100).toFixed(2)+"%";
	data[5].dodge= parseFloat((data[5].dodgeTimes/data[5].times)*100).toFixed(2)+"%";
	reload();
	scrollBottom();
	
	getHandOfJustice();
}
//嗜血
function getBloodthirsty(){
	var random = Math.random();
	var r=0;
	showAnger(-30);
	if(random<skillMissRate/100){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次嗜血,miss了'+'</p>');
		data[3].missTimes+=1;
	}
	random = Math.random();
	console.log(random+':'+mainHandDodgeRate/100);
	console.log(random<mainHandDodgeRate/100);
	if(random<mainHandDodgeRate/100){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次嗜血,被躲闪了'+'</p>');
		data[3].dodgeTimes+=1;
	}
	random = Math.random();
	if(random<skllCrit/100){
		r = (attr.ap*0.45)*2*1.2*(1-injuryFree)*deathWish;
		flurryTime=3;
		showFlurry();
		data[3].critTimes+=1;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次嗜血,暴击造成'+parseInt(r)+'点伤害</p>')
		unbridledWrath();
		effect();
	}else{
		r =(attr.ap*0.45)*(1-injuryFree)*deathWish;;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次嗜血,命中造成'+parseInt(r)+'点伤害</p>')
		unbridledWrath();
		effect();
	}
	data[3].total+=Math.round(r);
	data[3].times+=1;
	data[3].crit= parseFloat((data[3].critTimes/data[3].times)*100).toFixed(2)+"%";
	data[3].miss= parseFloat((data[3].missTimes/data[3].times)*100).toFixed(2)+"%";
	data[3].dodge= parseFloat((data[3].dodgeTimes/data[3].times)*100).toFixed(2)+"%";
	reload();
	scrollBottom();
	
	getHandOfJustice();
}
function getCyclone(){
	var random = Math.random();
	var r=0;
	showAnger(-25);
	if(random<skillMissRate/100){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次旋风斩,miss了'+'</p>');
		data[4].missTimes+=1
	}
	random = Math.random();
	if(random<mainHandDodgeRate/100){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次旋风斩,被躲闪了'+'</p>');
		data[4].dodgeTimes+=1
	}
	random = Math.random();
	if(random<skllCrit/100){
		r = randomNum(mainHandLowDamage,mainHandHighDamage)*2*1.2*(1-injuryFree)*deathWish;
		flurryTime=3;
		showFlurry();
		data[4].critTimes+=1;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次旋风斩,暴击造成'+parseInt(r)+'点伤害</p>')
		unbridledWrath();
		effect();
	}else{
		r =randomNum(mainHandLowDamage,mainHandHighDamage)*(1-injuryFree)*deathWish;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次旋风斩,命中造成'+parseInt(r)+'点伤害</p>')
		unbridledWrath();
		effect();
	}
	data[4].total+=Math.round(r);
	data[4].times+=1;
	data[4].crit= parseFloat((data[4].critTimes/data[4].times)*100).toFixed(2)+"%";
	data[4].miss= parseFloat((data[4].missTimes/data[4].times)*100).toFixed(2)+"%";
	data[4].dodge= parseFloat((data[4].dodgeTimes/data[4].times)*100).toFixed(2)+"%";
	reload();
	scrollBottom();
	
	getHandOfJustice();
}

function getHeroicStrike(){
	flurry();
	var random = Math.random();
	var r=0;
	if(random<skillMissRate/100){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次英勇打击,miss了'+'</p>');
		data[2].missTimes+=1;
	}
	random = Math.random();
	if(random<mainHandDodgeRate/100){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次英勇打击,被躲闪了'+'</p>')
		data[2].dodgeTimes+=1;
	}
	random = Math.random();
	if(random<skllCrit/100){
		r = (randomNum(mainHandLowDamage,mainHandHighDamage)+138)*2*1.2*(1-injuryFree)*deathWish;
		flurryTime=3;
		showFlurry();
		data[2].critTimes+=1;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次英勇打击,暴击造成'+parseInt(r)+'点伤害</p>');
		unbridledWrath();
		effect();
	}else{
		r = (randomNum(mainHandLowDamage,mainHandHighDamage)+138)*(1-injuryFree)*deathWish;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次英勇打击,命中造成'+parseInt(r)+'点伤害</p>');
		unbridledWrath();
		effect();
	}
	showAnger(-12);
	data[2].total+=Math.round(r);
	data[2].times+=1;
	data[2].crit= parseFloat((data[2].critTimes/data[2].times)*100).toFixed(2)+"%";
	data[2].miss= parseFloat((data[2].missTimes/data[2].times)*100).toFixed(2)+"%";
	data[2].dodge= parseFloat((data[2].dodgeTimes/data[2].times)*100).toFixed(2)+"%";
	reload();
	isHeroicStrike = false;
	scrollBottom();
	
	getHandOfJustice();
}

function getMainHandDamage(){
	flurry();
	var random = Math.random();
	var r = 0;
	var critTimes = 0;
	if(random<=mainHandMissSection){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次主手攻击,miss了'+'</p>');
		data[0].missTimes+=1;
	}else if(random>mainHandMissSection&&random<mainHandDodgeSection){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次主手攻击,被躲闪了'+'</p>');
		data[0].dodgeTimes+=1;
	}else if(random>mainHandDodgeSection&&random<mainHandDefSection){
		var a= randomNum(mainHandLowDamage,mainHandHighDamage);
		r = a*mainHandDef*(1-injuryFree)*deathWish;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次主手攻击,偏斜命中造成：'+parseInt(r)+"点伤害"+'</p>')
		effect();
	}else if(random>mainHandDefSection&&random<mainHandCritSection){
		var a= randomNum(mainHandLowDamage,mainHandHighDamage)*2;
		 r = a*(1-injuryFree)*deathWish;
		data[0].critTimes+=1;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次主手攻击,暴击造成：'+parseInt(r)+"点伤害"+'</p>')
		flurryTime=3;
		showFlurry();
		unbridledWrath();
		effect();
	}else{
		 r = randomNum(mainHandLowDamage,mainHandHighDamage)*(1-injuryFree)*deathWish;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次主手攻击,命中造成：'+parseInt(r)+"点伤害"+'</p>');
		unbridledWrath();
		effect();
	}
	
	showAnger(Math.round(r/30));
	data[0].total+=Math.round(r);
	data[0].times+=1;
	data[0].crit= parseFloat((data[0].critTimes/data[0].times)*100).toFixed(2)+"%";
	data[0].miss= parseFloat((data[0].missTimes/data[0].times)*100).toFixed(2)+"%";
	data[0].dodge= parseFloat((data[0].dodgeTimes/data[0].times)*100).toFixed(2)+"%";
	reload();
	scrollBottom();
	
	getHandOfJustice();
}
function getOffHandDamage(){
	flurry();
	var random = Math.random();
	var r = 0;
	var critTimes = 0;
	if(random<=offHandMissSection){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次副手攻击,miss了'+'</p>');
		data[1].missTimes+=1;
	}else if(random>offHandMissSection&&random<offHandDodgeSection){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次副手攻击,被躲闪了'+'</p>')
		data[1].dodgeTimes+=1;
	}else if(random>offHandDodgeSection&&random<offHandDefSection){
		r = randomNum(offHandLowDamage,offHandHighDamage)*offHandDef*(1-injuryFree)*deathWish;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次副手攻击,偏斜命中造成：'+parseInt(r)+"点伤害"+'</p>');
		unbridledWrath();
		effect();
	}else if(random>offHandDefSection&&random<offHandCritSection){
		 r = randomNum(offHandLowDamage,offHandHighDamage)*2*(1-injuryFree)*deathWish;
		data[1].critTimes+=1;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次副手攻击,暴击造成：'+parseInt(r)+"点伤害"+'</p>')
		flurryTime=3;
		showFlurry();
		unbridledWrath();
		effect();
	}else{
		 r = randomNum(offHandLowDamage,offHandHighDamage)*(1-injuryFree)*deathWish;
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次副手攻击,命中造成：'+parseInt(r)+"点伤害"+'</p>')
		unbridledWrath();
		effect();
	}
	
	showAnger(Math.round(r/30));
	data[1].total+=parseInt(r);
	data[1].times+=1;
	data[1].crit= parseFloat((data[1].critTimes/data[1].times)*100).toFixed(2)+"%";
	data[1].miss= parseFloat((data[1].missTimes/data[1].times)*100).toFixed(2)+"%";
	data[1].dodge= parseFloat((data[1].dodgeTimes/data[1].times)*100).toFixed(2)+"%";
	reload();
	scrollBottom();
	
	getHandOfJustice();
}

function effect(){
	bonereaverEffect();
}
var bonereaverEffectTimes=0;
var bonereaverEffectTimeout;
function bonereaverEffect(){
	if($('#mainHand').attr('code')!='17076'){
		return;
	}
	
	var random = Math.random();
	if(random<0.20){
		if(bonereaverEffectTimes<3){
			bonereaverEffectTimes++;
			
		}
		injuryFree = getDr(armor-bonereaverEffectTimes*700);
		$('#record').append(startTime.Format('hh:mm:ss')+"<font color='red'>触发率削骨特效,此时层数："+bonereaverEffectTimes+"</font>");
		console.log("此时护甲："+(armor-bonereaverEffectTimes*700));
		console.log("此时免伤："+injuryFree);
		clearTimeout(bonereaverEffectTimeout);
		bonereaverEffectTimeout=setTimeout(function(){
			bonereaverEffectTimes=0;
			injuryFree = getDr(armor);
			$('#record').append(startTime.Format('hh:mm:ss')+"<font color='red'>削骨特效时间结束</font>");
			console.log("此时护甲："+armor);
			console.log("此时免伤："+injuryFree);
		},10*1000);
	}
	
	
}

//正义之手
function getHandOfJustice(){
	var handOfJusticeRandom = Math.random();
	if(handOfJusticeRandom<0.02){
		var random = Math.random();
		var r = 0;
		var critTimes = 0;
		
		if(offHandSpeed==0){
			if(random<=mainHandMissSection){
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发正义之手,miss了'+'</p>');
				data[6].missTimes+=1;
			}else if(random>mainHandMissSection&&random<mainHandDodgeSection){
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发正义之手,被躲闪了'+'</p>')
				data[6].dodgeTimes+=1;
			}else if(random>mainHandDodgeSection&&random<mainHandDefSection){
				var a= randomNum(mainHandLowDamage,mainHandHighDamage);
				r = a*mainHandDef*(1-injuryFree)*deathWish;
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'发动一次主手攻击,偏斜命中造成：'+parseInt(r)+"点伤害"+'</p>')
			}else if(random>mainHandDefSection&&random<mainHandCritSection){
				var a= randomNum(mainHandLowDamage,mainHandHighDamage)*2;
				 r = a*(1-injuryFree)*deathWish;
				data[6].critTimes+=1;
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发正义之手,暴击造成：'+parseInt(r)+"点伤害"+'</p>')
				flurryTime=3;
				showFlurry();
				unbridledWrath();
			}else{
				 r = randomNum(mainHandLowDamage,mainHandHighDamage)*(1-injuryFree)*deathWish;
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发正义之手,命中造成：'+parseInt(r)+"点伤害"+'</p>')
				unbridledWrath();
			}
		}else{
			if(random<=offHandMissSection){
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发正义之手,miss了'+'</p>');
				data[6].missTimes+=1;
			}else if(random>offHandMissSection&&random<offHandDodgeSection){
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发正义之手,被躲闪了'+'</p>')
				data[6].dodgeTimes+=1;
			}else if(random>offHandDodgeSection&&random<offHandDefSection){
				r = randomNum(offHandLowDamage,offHandHighDamage)*offHandDef*(1-injuryFree)*deathWish;
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发正义之手,偏斜命中造成：'+parseInt(r)+"点伤害"+'</p>');
				unbridledWrath();
			}else if(random>offHandDefSection&&random<offHandCritSection){
				 r = randomNum(offHandLowDamage,offHandHighDamage)*2*(1-injuryFree)*deathWish;
				data[1].critTimes+=1;
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发正义之手,暴击造成：'+parseInt(r)+"点伤害"+'</p>')
				flurryTime=3;
				showFlurry();
				unbridledWrath();
			}else{
				 r = randomNum(offHandLowDamage,offHandHighDamage)*(1-injuryFree)*deathWish;
				$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发正义之手,命中造成：'+parseInt(r)+"点伤害"+'</p>')
				unbridledWrath();
			}
		}
		
		showAnger(Math.round(r/30));
		data[6].total+=parseInt(r);
		data[6].times+=1;
		data[6].crit= parseFloat((data[6].critTimes/data[6].times)*100).toFixed(2)+"%";
		data[6].miss= parseFloat((data[6].missTimes/data[6].times)*100).toFixed(2)+"%";
		data[6].dodge= parseFloat((data[6].dodgeTimes/data[6].times)*100).toFixed(2)+"%";
		reload();
		scrollBottom();
	}
}

function flurry(){
	flurryTime--;
	if(flurryTime>=1){
		mainHandSpeed=parseFloat(1/(1.3/attr.mainHandSpeed))*1000;
		offHandSpeed=parseFloat(1/(1.3/attr.offHandSpeed))*1000;
	}else{
		flurryTime=0;
		mainHandSpeed = attr.mainHandSpeed*1000;
		offHandSpeed=attr.offHandSpeed*1000;
	}
	showFlurry();
}
function showFlurry(){
	$('#flurry').text("乱舞:"+flurryTime);
}
function unbridledWrath(){
	var random = Math.random();
	if(random<0.4){
		$('#record').append('<p>'+startTime.Format('hh:mm:ss')+'触发怒不可遏怒气,产生一点怒气'+'</p>')
		showAnger(1);
	}
}

function getDodgeRate(skills) {
	return parseFloat(6.5 - 0.1*(skills-300));
}
function getMissRate(hitRate,skills) {
	var miss=0;
	if(skills<305) {
		if(hitRate>=1){
			miss =  parseFloat(27-0.2*(skills-300)-hitRate+1);
		}else{
			miss =  parseFloat(27-0.2*(skills-300)-hitRate);
		}
	}else if(skills>= 305){
	    miss =  parseFloat(27-2-0.1*(skills-305)-hitRate);
	}
	if(miss<0){
		miss = 0;
	}
	return miss;
}
function getSkillMissRate(hitRate,skills) {
	var miss=0;
	if(skills<305) {
		if(hitRate>=1){
			miss =  parseFloat(8-0.2*(skills-300))-hitRate+1;
		}else{
			miss =  parseFloat(8-0.2*(skills-300))-hitRate;
		}
	}else if(skills>= 305){
	    miss =  parseFloat(8-2-0.1*(skills-305))-hitRate;
	}
	if(miss<0){
		miss = 0;
	}
	return miss;
}

function getDef( skills) {
	var def=0;
	if(skills<308) {
		def  =  parseFloat((1.2-0.03*(315-skills)+1.3-0.05*(315-skills))/2);
	}else {
		def=0.95;
	}
	return def;
}

function randomNum(minNum, maxNum) {
	  switch (arguments.length) {
	    case 1:
	      return parseInt(Math.random() * minNum + 1, 10);
	      break;
	    case 2:
	      return parseInt(Math.random() * ( maxNum - minNum + 1 ) + minNum, 10);
	      //或者 Math.floor(Math.random()*( maxNum - minNum + 1 ) + minNum );
	      break;
	    default:
	      return 0;
	      break;
	  }
	}
function fomatFloat(num,n){   
    var f = parseFloat(num);
    if(isNaN(f)){
        return false;
    }   
    f = Math.round(num*Math.pow(10, n))/Math.pow(10, n); // n 幂   
    var s = f.toString();
    var rs = s.indexOf('.');
    //判定如果是整数，增加小数点再补0
    if(rs < 0){
        rs = s.length;
        s += '.'; 
    }
    while(s.length <= rs + n){
        s += '0';
    }
    return s;
}  
function scrollBottom(){
	  var scrollHeight = $('#record').prop("scrollHeight");
      $('#record').scrollTop(scrollHeight,200);

}
Date.prototype.Format = function(formatStr)   
{   
    var str = formatStr;   
    var Week = ['日','一','二','三','四','五','六'];  
   
    str=str.replace(/yyyy|YYYY/,this.getFullYear());   
    str=str.replace(/yy|YY/,(this.getYear() % 100)>9?(this.getYear() % 100).toString():'0' + (this.getYear() % 100));   
   
    str=str.replace(/MM/,this.getMonth()>9?this.getMonth().toString():'0' + this.getMonth());   
    str=str.replace(/M/g,this.getMonth());   
   
    str=str.replace(/w|W/g,Week[this.getDay()]);   
   
    str=str.replace(/dd|DD/,this.getDate()>9?this.getDate().toString():'0' + this.getDate());   
    str=str.replace(/d|D/g,this.getDate());   
   
    str=str.replace(/hh|HH/,this.getHours()>9?this.getHours().toString():'0' + this.getHours());   
    str=str.replace(/h|H/g,this.getHours());   
    str=str.replace(/mm/,this.getMinutes()>9?this.getMinutes().toString():'0' + this.getMinutes());   
    str=str.replace(/m/g,this.getMinutes());   
   
    str=str.replace(/ss|SS/,this.getSeconds()>9?this.getSeconds().toString():'0' + this.getSeconds());   
    str=str.replace(/s|S/g,this.getSeconds());   
   
    return str;   
}   
