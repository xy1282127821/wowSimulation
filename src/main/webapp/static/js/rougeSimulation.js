/**
 * 目录
 * 1.startSimulation 开始模拟
 * 2.stopSimulation 结束模拟
 * 3.initSimulation 模拟初始化
 * 4.rougeSimulation 模拟定时器
 * 5.getDr 护甲减免计算
 */

var attr;
var mainHand;
var offHand;
var mainHandSpeed;
var offHandSpeed;
var energyRecovery;
var sinisterStrike;
var sliceAndDice;
var eviscerate;
var specialEffects;
var adrenalineRush;
var bladeFlurry; 
var startTime; //模拟开始时间
var now; //当前时间
var ap;  // 当前AP
var star;  //星
var energy; //能量
var energyRecoveryPer;//每2秒能量回复
var issliceAndDice=false;
var isBladeFlurry=false
const  publicCD=1*1000; //公共CD
var lastSkillTime=0; //最后一次使用技能的时间
var isAuto=false; //是否自动模拟
function startSimulation(){
	if(attr){
		initSimulation();
		startTime =  new Date().getTime();
		initDatagrid();
		intervalset=setInterval(rougeSimulation,10);
	}else{
		alert('人物属性没加载成功,请稍等');
	}
	
}
function stopSimulation(){
	$('#record').append('<p>'+now.Format('hh:mm:ss')+'停止模拟...'+'</p>')
	clearInterval(intervalset);
	scrollBottom();
}
function initSimulation(){
	injuryFree = getDr(armor);
	mainHand = new MainHandAttack(attr.mainHandWeapon,attr.mainHandSkill);
	offHand = new OffHandAttack(attr.offHandWeapon,attr.offHandSkill);
	energyRecovery = new EnergyRecovery();
	sinisterStrike = new SinisterStrike(attr.mainHandSkill);
	eviscerate = new Eviscerate(attr.mainHandSkill);
	sliceAndDice = new SliceAndDice();
	adrenalineRush = new AdrenalineRush();
	specialEffects = new SpecialEffects();
	bladeFlurry = new BladeFlurry();
	ap = attr.ap;
	mainHandSpeed = attr.mainHandSpeed;
	offHandSpeed = attr.offHandSpeed;
	energy=100;
	star=0;
	energyRecoveryPer=20;
	isAuto = $('#auto').is(':checked');
	console.log(isAuto);
	if(isAuto==false){
		$(document).keydown(function(event){
			var date = new Date();
			if(event.keyCode==49){
				sinisterStrike.cast(date); 
			}else if(event.keyCode==51){
				sliceAndDice.cast(date); //施放切割
			}else if(event.keyCode==50){
				eviscerate.attack(date);
			}else if(event.keyCode==52){
				adrenalineRush.cast(date); 
			}else if(event.keyCode==53){
				bladeFlurry.cast(date); //施法乱舞
			}
		});
	}
}
function rougeSimulation(){
	now = new Date();
	energyRecovery.recovery(now);
	mainHand.attck(now);
	offHand.attck(now);
	sliceAndDice.showSliceAndDice(now);
	if(isAuto){
		sliceAndDice.autoCast(now); //施放切割
		sinisterStrike.attck(now); //施放邪恶打击
		eviscerate.autoAttack(now); //施法剔骨
		adrenalineRush.cast(now); //施法冲动
		bladeFlurry.cast(now); //施法乱舞
	}
}



function getDr(ac){
	 if(ac<0){
		 return 0;
	 }
	 var dr=ac/(ac+85*60+400) ;
	 return dr;
}
class CommonAttack{
	constructor(skills){
		this.skills= skills;
		this.def=this.getDef(this.skills);
		this.missRate=this.getMissRate(this.skills);
		this.dogeRate=this.getDodgeRate(this.skills);
		this.missSection = parseFloat(this.missRate/100);
		this.dodgeSection = parseFloat(this.missRate/100+this.dogeRate/100);
		this.defSection = parseFloat(this.dodgeSection+0.4);
		this.critSection = parseFloat(this.defSection+(attr.mainHandCrit-3)/100);
	}
	getDef(skills) {
		var def=0;
		if(skills<308) {
			def  =  parseFloat((1.2-0.03*(315-skills)+1.3-0.05*(315-skills))/2);
		}else {
			def=0.95;
		}
		return def;
	}
	getMissRate(skills) {
		var hitRate = attr.hitRate;
		var miss=0;
		if(skills<305) {
			if(hitRate>=1){
				miss =  parseFloat(27-0.2*(skills-300)-hitRate+1);
			}else{
				miss =  parseFloat(27-0.2*(skills-300)-hitRate);
			}
		}else if(skills>= 305){
		    miss =  parseFloat(27-2-0.1*(skills-305)-hitRate);
		}
		if(miss<0){
			miss = 0;
		}
		return miss;
	}
	getDodgeRate(skills) {
		return parseFloat(6.5 - 0.1*(skills-300));
	}
}
class MainHandAttack extends CommonAttack{
	constructor(mainHandWeapon,skill){
		super(skill);
		this.weapon = mainHandWeapon;
		this.lastAttackTime=0;
		var equipDamage = mainHandWeapon.equipDamage.replace('伤害').trim();
		this.lowDamage = parseInt(equipDamage.split('-')[0]);
		this.highDamage = parseInt(equipDamage.split('-')[1]);
	}
	attck(now){
		if(this.lastAttackTime==0||(now-this.lastAttackTime)>=mainHandSpeed*1000){
			this.lastAttackTime=now;
			var random = Math.random();
			var r = 0;
			if(random<=this.missSection){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次主手攻击,miss了'+'</p>');
				data[0].missTimes+=1;
			}else if(random>this.missSection&&random<=this.dodgeSection){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次主手攻击,被躲闪了'+'</p>');
				data[0].dodgeTimes+=1;
			}else if(random>this.dodgeSection&&random<=this.defSection){
				r = Math.round(this.getDamage()*this.def*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次主手攻击,偏斜命中造成了'+r+'点伤害'+'</p>');
				specialEffects.trigger();
			}else if(random>this.defSection&&random<=this.critSection){
				r =Math.round(this.getDamage()*2*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次主手攻击,暴击命中造成'+r+'点伤害'+'</p>');
				data[0].critTimes+=1;
				specialEffects.trigger();
			}else{
				r= Math.round(this.getDamage()*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次主手攻击,命中目标造成'+r+'点伤害'+'</p>');
				specialEffects.trigger();
			}
			data[0].total+=r;
			data[0].times+=1;
			data[0].crit= parseFloat((data[0].critTimes/data[0].times)*100).toFixed(2)+"%";
			data[0].miss= parseFloat((data[0].missTimes/data[0].times)*100).toFixed(2)+"%";
			data[0].dodge= parseFloat((data[0].dodgeTimes/data[0].times)*100).toFixed(2)+"%";
			reload();
			scrollBottom();
		}
	}
	getDamage(){
		if(buffCodes.indexOf('buff16')>0){
			return randomNum(this.lowDamage,this.highDamage)+ap*attr.mainHandSpeed/14+8;
		}else{
			return randomNum(this.lowDamage,this.highDamage)+ap*attr.mainHandSpeed/14;
		}
	}
	swordEffect(){
		var random = Math.random();
		if(random<=0.05){
			random = Math.random();
			var r = 0;
			if(random<=this.missSection){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'触发了剑专精,miss了'+'</p>');
				data[0].missTimes+=1;
			}else if(random>this.missSection&&random<=this.dodgeSection){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'触发了剑专精,被躲闪了'+'</p>');
				data[0].dodgeTimes+=1;
			}else if(random>this.dodgeSection&&random<=this.defSection){
				r = Math.round(this.getDamage()*this.def*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'触发了剑专精,偏斜命中造成了'+r+'点伤害'+'</p>');
				specialEffects.trigger();
			}else if(random>this.defSection&&random<=this.critSection){
				r =Math.round(this.getDamage()*2*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'触发了剑专精,暴击命中造成'+r+'点伤害'+'</p>');
				data[0].critTimes+=1;
				specialEffects.trigger();
			}else{
				r= Math.round(this.getDamage()*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'触发了剑专精,命中目标造成'+r+'点伤害'+'</p>');
				specialEffects.trigger();
			}
			data[5].total+=r;
			data[5].times+=1;
			data[5].crit= parseFloat((data[5].critTimes/data[5].times)*100).toFixed(2)+"%";
			data[5].miss= parseFloat((data[5].missTimes/data[5].times)*100).toFixed(2)+"%";
			data[5].dodge= parseFloat((data[5].dodgeTimes/data[5].times)*100).toFixed(2)+"%";
			reload();
			scrollBottom();
		}
	}
}
class OffHandAttack extends CommonAttack{
	constructor(offHandWeapon,skill){
		super(skill);
		this.weapon = offHandWeapon;
		this.lastAttackTime=0;
		var equipDamage = offHandWeapon.equipDamage.replace('伤害').trim();
		this.lowDamage = parseInt(equipDamage.split('-')[0]);
		this.highDamage = parseInt(equipDamage.split('-')[1]);
		this.critSection = parseFloat(this.defSection+(attr.offHandCrit-3)/100);
	}
	attck(now){
		if(this.lastAttackTime==0||(now-this.lastAttackTime)>=offHandSpeed*1000){
			this.lastAttackTime=now;
			var random = Math.random();
			var r = 0;
			if(random<=this.missSection){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次副手攻击,miss了'+'</p>');
				data[1].missTimes+=1;
			}else if(random>this.missSection&&random<=this.dodgeSection){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次副手攻击,被躲闪了'+'</p>');
				data[1].dodgeTimes+=1;
			}else if(random>this.dodgeSection&&random<=this.defSection){
				r = Math.round(this.getDamage()*this.def*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次副手攻击,偏斜命中造成了'+r+'点伤害'+'</p>');
				specialEffects.trigger();
			}else if(random>this.defSection&&random<=this.critSection){
				r =Math.round(this.getDamage()*2*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次副手攻击,暴击命中造成'+r+'点伤害'+'</p>');
				data[1].critTimes+=1;
				specialEffects.trigger();
			}else{
				r= Math.round(this.getDamage()*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次副手攻击,命中目标造成'+r+'点伤害'+'</p>');
				specialEffects.trigger();
			}
			data[1].total+=parseInt(r);
			data[1].times+=1;
			data[1].crit= parseFloat((data[1].critTimes/data[1].times)*100).toFixed(2)+"%";
			data[1].miss= parseFloat((data[1].missTimes/data[1].times)*100).toFixed(2)+"%";
			data[1].dodge= parseFloat((data[1].dodgeTimes/data[1].times)*100).toFixed(2)+"%";
			reload();
			scrollBottom();
		}
	}
	getDamage(){
		if(buffCodes.indexOf('buff16')>0){
			return (randomNum(this.lowDamage,this.highDamage)+ap*attr.mainHandSpeed/14+8)/2*1.5;
		}else{
			return (randomNum(this.lowDamage,this.highDamage)+ap*attr.mainHandSpeed/14)/2*1.5;
		}
	}
}


class EnergyRecovery{
	constructor(){
		this.lastRecoveryTime = 0;
	}
	recovery(now){
		if(now-this.lastRecoveryTime>2*1000){
			showEnergy(energyRecoveryPer);
			this.lastRecoveryTime = now;
			$('#record').append('<p>'+now.Format('hh:mm:ss')+'回复了'+energyRecoveryPer+'能量'+'</p>');
			scrollBottom();
		}
	}
}
class Skill{
	constructor(skills){
		this.skills= skills;
		this.missRate=this.getSkillMissRate(this.skills);
		this.dogeRate=this.getDodgeRate(this.skills);
	}
	getSkillMissRate(skills) {
		var hitRate = attr.hitRate;
		var miss=0;
		if(skills<305) {
			if(hitRate>=1){
				miss =  parseFloat(8-0.2*(skills-300))-hitRate+1;
			}else{
				miss =  parseFloat(8-0.2*(skills-300))-hitRate;
			}
		}else if(skills>= 305){
		    miss =  parseFloat(8-2-0.1*(skills-305))-hitRate;
		}
		if(miss<0){
			miss = 0;
		}
		return miss;
	}
	getDodgeRate(skills) {
		return parseFloat(6.5 - 0.1*(skills-300));
	}
}
/**
 * 邪恶打击
 */
class SinisterStrike extends Skill{
	constructor(skills){
		super(skills);
		var equipDamage = attr.mainHandWeapon.equipDamage.replace('伤害').trim();
		this.lowDamage = parseInt(equipDamage.split('-')[0]);
		this.highDamage = parseInt(equipDamage.split('-')[1]);
		this.active=false;
		
	}
	getDamage(){
		if(buffCodes.indexOf('buff16')>0){
			return (randomNum(this.lowDamage,this.highDamage)+ap*2.4/14+8+68);
		}else{
			return (randomNum(this.lowDamage,this.highDamage)+ap*2.4/14+68);
		}
	}
	cast(now){
		if(energy<40){
			return;
		}
		if(now-lastSkillTime<publicCD){
			return;
		}
		showEnergy(-40);
		showStar(1);
		data[3].times+=1;
		var r;
		lastSkillTime = now;
		var random = Math.random();
		if(random<=this.missRate){
			$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次邪恶打击,miss了'+'</p>');
			data[3].missTimes+=1;
			data[3].miss= parseFloat((data[3].missTimes/data[3].times)*100).toFixed(2)+"%";
			data[3].dodge= parseFloat((data[3].dodgeTimes/data[3].times)*100).toFixed(2)+"%";
			return;
		}
		random = Math.random();
		if(random<=this.dogeRate/100){
			$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次邪恶打击,被躲闪了'+'</p>');
			data[3].dodgeTimes +=1;
			return;
		}
		random = Math.random();
		if(random<=(attr.mainHandCrit-3)/100){
			r =Math.round(this.getDamage()*2*1.3*(1-injuryFree));
			$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次邪恶打击,暴击造成'+r+'伤害</p>');
			data[3].critTimes+=1;
			specialEffects.trigger();
		}else{
			r =Math.round(this.getDamage()*(1-injuryFree));
			$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次邪恶打击,命中'+r+'伤害</p>');
			specialEffects.trigger();
		}
		data[3].total+=r;
		data[3].crit= parseFloat((data[3].critTimes/data[3].times)*100).toFixed(2)+"%";
		reload();
		scrollBottom();
		this.paint();
	}
	attck(now){
		if(star<5){
			this.cast(now);
		}
	}
	paint(){
		 if(this.active)  
	            return; 
		 var canvas = document.getElementById('sinisterStrikeCanvas');
		 var context = canvas.getContext("2d");  
	     var canvasWidth = canvas.width,  
         canvasHeight = canvas.height,  
         cx = canvasWidth/2,  
         cy = canvasHeight/2;     
	     
	     //画一个半透明的灰色背景  
         context.beginPath();  
         context.clearRect(0,0,canvasWidth,canvasHeight);  
         context.moveTo(0,0);  
         context.fillStyle = 'rgba(0,0,0,.7)';  
         context.fillRect(0,0,canvasWidth,canvasHeight);  
         context.fill();  
         
         var speed = 50,  
         step = 2*Math.PI/20,  
         start = 3*Math.PI/2,  
         begin = start,  
         end = start + step,  
         len = canvas.width > canvas.height ? canvas.width:canvas.height,  
         r = Math.round(0.8*len);  
	     if(canvas.getAttribute('sec')){  
	         speed = Math.round(50*canvas.getAttribute('sec')/9);  
	     }  
	     var _this = this;
	     context.beginPath();  
	        context.moveTo(cx, cy);  
	        context.fillStyle = 'rgb(0,0,0)';  
	        context.globalCompositeOperation = "destination-out";  
	        var that = $('#sinisterStrikeCanvas');;  
	  
	        var timer = null;  
	        var doPaint = function(){  
	            context.arc(cx, cy, r, start, end, false);  
	            context.fill();  
	            start = end;  
	            end = end + step;  
	            if(end - begin > 2*Math.PI){  
	                clearTimeout(timer);  
	                context.globalCompositeOperation = "source-over";  
	                _this.active = false;  
	                return;  
	            }   
	            else  
	               timer = setTimeout(doPaint,speed);  
	              
	        }  
	        doPaint();  
	}
}
/**
 * 剔骨
 */
class Eviscerate extends Skill{
	constructor(skills){
		super(skills);
	}
	getDamage(){
			switch(star){
				 case 1:{
						return (randomNum(199,295)+ap*0.15);
				 }
				 case 2:{
						return (randomNum(350,446)+ap*0.15);
				 }
				 case 3:{
						return (randomNum(501,597)+ap*0.15);
				 }
				 case 4:{
						return (randomNum(652,748)+ap*0.15);
				 }
				 case 5:{
						return (randomNum(803,899)+ap*0.15);
				 }
			}
	}
	autoAttack(now){
		if(star==5&&((sliceAndDice.sliceAndDiceTime*1000)-(now-sliceAndDice.sliceAndDiceStartTime))>=6*1000){
			this.attack(now)
		}
	}
	attack(now){
		if(now-lastSkillTime>publicCD&&energy>35&&star>0){
			showEnergy(-35);
			data[4].times+=1;
			var r;
			lastSkillTime = now;
			var random = Math.random();
			if(random<=this.missRate){
				data[4].miss= parseFloat((data[4].missTimes/data[4].times)*100).toFixed(2)+"%";
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次剔骨,miss了'+'</p>');
				data[4].missTimes+=1;
			}
			random = Math.random();
			if(random<=this.dogeRate/100){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次剔骨,被躲闪了'+'</p>');
				data[4].dodgeTimes+=1;
				data[4].dodge= parseFloat((data[4].dodgeTimes/data[4].times)*100).toFixed(2)+"%";
			}
			random = Math.random();
			if(random<=(attr.mainHandCrit-3)/100){
				data[4].crit= parseFloat((data[4].critTimes/data[4].times)*100).toFixed(2)+"%";
				r =Math.round(this.getDamage()*2*1.3*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次剔骨,暴击造成'+r+'伤害</p>');
				data[4].critTimes+=1;
				specialEffects.trigger();
				specialEffects.ruthless();
				specialEffects.relentlessStrikes(star);
				showStar(-star);
			}else{
				r =Math.round(this.getDamage()*(1-injuryFree));
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'发起一次剔骨,命中'+r+'伤害</p>');
				specialEffects.trigger();
				specialEffects.ruthless();
				specialEffects.ruthless();
				specialEffects.relentlessStrikes(5);
				showStar(-star);
			}
			data[4].total+=r;
			reload();
			scrollBottom();
			this.paint();
		}
		
	}
	paint(){
		 if(this.active)  
	            return; 
		 var canvas = document.getElementById('eviscerateCanvas');
		 var context = canvas.getContext("2d");  
	     var canvasWidth = canvas.width,  
        canvasHeight = canvas.height,  
        cx = canvasWidth/2,  
        cy = canvasHeight/2;     
	     
	     //画一个半透明的灰色背景  
        context.beginPath();  
        context.clearRect(0,0,canvasWidth,canvasHeight);  
        context.moveTo(0,0);  
        context.fillStyle = 'rgba(0,0,0,.7)';  
        context.fillRect(0,0,canvasWidth,canvasHeight);  
        context.fill();  
        
        var speed = 50,  
        step = 2*Math.PI/20,  
        start = 3*Math.PI/2,  
        begin = start,  
        end = start + step,  
        len = canvas.width > canvas.height ? canvas.width:canvas.height,  
        r = Math.round(0.8*len);  
	     if(canvas.getAttribute('sec')){  
	         speed = Math.round(50*canvas.getAttribute('sec')/9);  
	     }  
	     var _this = this;
	     context.beginPath();  
	        context.moveTo(cx, cy);  
	        context.fillStyle = 'rgb(0,0,0)';  
	        context.globalCompositeOperation = "destination-out";  
	        var that = $('#eviscerateCanvas');;  
	  
	        var timer = null;  
	        var doPaint = function(){  
	            context.arc(cx, cy, r, start, end, false);  
	            context.fill();  
	            start = end;  
	            end = end + step;  
	            if(end - begin > 2*Math.PI){  
	                clearTimeout(timer);  
	                context.globalCompositeOperation = "source-over";  
	                _this.active = false;  
	                return;  
	            }   
	            else  
	               timer = setTimeout(doPaint,speed);  
	              
	        }  
	        doPaint();  
	}
}
/**
 * 切割
 */
class SliceAndDice{
	constructor(){
		this.isExist=false;
		this.sliceAndDiceTimeout;
		this.sliceAndDiceStartTime=0;
		this.sliceAndDiceTime=0;
	}
	autoCast(now){
		if(issliceAndDice==false){
			this.cast(now);
		}
	}
	cast(now){
		if(now-lastSkillTime<publicCD){
			return;
		}
		if(star>0){
			lastSkillTime = now;
			issliceAndDice=true;
			if(isBladeFlurry){
				offHandSpeed = 1/(1.3*1.2/attr.offHandSpeed);
				mainHandSpeed = 1/(1.3*1.2/attr.mainHandSpeed);
			}else{
				offHandSpeed = 1/(1.3/attr.offHandSpeed);
				mainHandSpeed = 1/(1.3/attr.mainHandSpeed);
			}
			console.log("主手速度："+mainHandSpeed+",副手速度"+offHandSpeed)
			switch(star){
				 case 1:{
					 this.sliceAndDiceTime = 9*1.45;
					 break;
				 }
				 case 2:{
					 this.sliceAndDiceTime = 12*1.45;
					 break;
				 }
				 case 3:{
					 this.sliceAndDiceTime = 15*1.45;
					 break;
				 }
				 case 4:{
					 this.sliceAndDiceTime = 18*1.45;
					 break;
				 }
				 case 5:{
					 this.sliceAndDiceTime = 21*1.45;
					 break;
				 }
			}
			specialEffects.ruthless();
			specialEffects.relentlessStrikes(star);
			showStar(-star);
			showEnergy(-25);
			this.sliceAndDiceStartTime = now;
			$('#record').append(now.Format('hh:mm:ss')+'切割开始：'+this.sliceAndDiceTime);
			if(this.sliceAndDiceTimeout){
				clearTimeout(this.sliceAndDiceTimeout);
			}
			this.paint();
			this.sliceAndDiceTimeout = setTimeout(function(){
				issliceAndDice=false;
				if(isBladeFlurry){
					mainHandSpeed = 1/(1.2/attr.mainHandSpeed);
					offHandSpeed = 1/(1.2/attr.offHandSpeed);
				}else{
					mainHandSpeed=attr.mainHandSpeed;
					offHandSpeed=attr.offHandSpeed;
				}
				console.log("主手速度："+mainHandSpeed+",副手速度"+offHandSpeed)
				$('#record').append(now.Format('hh:mm:ss')+'切割结束');
			},1000*this.sliceAndDiceTime);
			
		}
	}
	showSliceAndDice(now){
		var time = this.sliceAndDiceTime-(now-this.sliceAndDiceStartTime)/1000;
		if(time<0){
			time=0;
			$('#flurry').text('切割：'+parseInt(time)+'秒');
		}else{
			$('#flurry').text('切割：'+parseInt(time)+'秒');
		}
	}
	paint(){
		 if(this.active)  
	            return; 
		 var canvas = document.getElementById('siceAndDiceCanvas');
		 var context = canvas.getContext("2d");  
	     var canvasWidth = canvas.width,  
       canvasHeight = canvas.height,  
       cx = canvasWidth/2,  
       cy = canvasHeight/2;     
	     
	     //画一个半透明的灰色背景  
       context.beginPath();  
       context.clearRect(0,0,canvasWidth,canvasHeight);  
       context.moveTo(0,0);  
       context.fillStyle = 'rgba(0,0,0,.7)';  
       context.fillRect(0,0,canvasWidth,canvasHeight);  
       context.fill();  
       
       var speed = 50,  
       step = 2*Math.PI/20,  
       start = 3*Math.PI/2,  
       begin = start,  
       end = start + step,  
       len = canvas.width > canvas.height ? canvas.width:canvas.height,  
       r = Math.round(0.8*len);  
	     if(canvas.getAttribute('sec')){  
	         speed = Math.round(50*canvas.getAttribute('sec')/9);  
	     }  
	     var _this = this;
	     context.beginPath();  
	        context.moveTo(cx, cy);  
	        context.fillStyle = 'rgb(0,0,0)';  
	        context.globalCompositeOperation = "destination-out";  
	        var that = $('#siceAndDiceCanvas');;  
	  
	        var timer = null;  
	        var doPaint = function(){  
	            context.arc(cx, cy, r, start, end, false);  
	            context.fill();  
	            start = end;  
	            end = end + step;  
	            if(end - begin > 2*Math.PI){  
	                clearTimeout(timer);  
	                context.globalCompositeOperation = "source-over";  
	                _this.active = false;  
	                return;  
	            }   
	            else  
	               timer = setTimeout(doPaint,speed);  
	              
	        }  
	        doPaint();  
	}
	
}
/**
 * 冲动
 */
class AdrenalineRush{
	constructor(){
		this.cd = 5*60;
		this.lastCastTime=0;
	}
	cast(now){
		if(now-lastSkillTime<publicCD){
			return;
		}
		if(now-this.lastCastTime>=this.cd*1000){
			$('#record').append('<p>'+now.Format('hh:mm:ss')+'<font color="red">使用冲动</font>'+'</p>');
			scrollBottom();
			energyRecoveryPer = 40;
			this.lastCastTime= now;
			lastSkillTime = now;
			this.paint();
			setTimeout(function(){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'<font color="red">冲动结束</font>'+'</p>');
				scrollBottom();
				energyRecoveryPer = 20;
			},15*1000);
		}
	}
	paint(){
		 if(this.active)  
	            return; 
		 var canvas = document.getElementById('adrenalineRushCanvas');
		 var context = canvas.getContext("2d");  
	     var canvasWidth = canvas.width,  
	      canvasHeight = canvas.height,  
	      cx = canvasWidth/2,  
	      cy = canvasHeight/2;     
		     
		     //画一个半透明的灰色背景  
	      context.beginPath();  
	      context.clearRect(0,0,canvasWidth,canvasHeight);  
	      context.moveTo(0,0);  
	      context.fillStyle = 'rgba(0,0,0,.7)';  
	      context.fillRect(0,0,canvasWidth,canvasHeight);  
	      context.fill();  
	      
	      var speed = 15*1000,  
	      step = 2*Math.PI/20,  
	      start = 3*Math.PI/2,  
	      begin = start,  
	      end = start + step,  
	      len = canvas.width > canvas.height ? canvas.width:canvas.height,  
	      r = Math.round(0.8*len);  
		     if(canvas.getAttribute('sec')){  
		         speed = Math.round(50*canvas.getAttribute('sec')/9);  
		     }  
		     var _this = this;
		     context.beginPath();  
		        context.moveTo(cx, cy);  
		        context.fillStyle = 'rgb(0,0,0)';  
		        context.globalCompositeOperation = "destination-out";  
		        var that = $('#adrenalineRushCanvas');;  
		  
		        var timer = null;  
		        var doPaint = function(){  
		            context.arc(cx, cy, r, start, end, false);  
		            context.fill();  
		            start = end;  
		            end = end + step;  
		            if(end - begin > 2*Math.PI){  
		                clearTimeout(timer);  
		                context.globalCompositeOperation = "source-over";  
		                _this.active = false;  
		                return;  
		            }   
		            else  
		               timer = setTimeout(doPaint,speed);  
		              
		        }  
		        doPaint();  
	}
}
/**
 * 乱舞
 */
class BladeFlurry{
	constructor(){
		this.cd = 2*60;
		this.lastCastTime=0;
	}
	cast(now){
		if(now-lastSkillTime<publicCD){
			return;
		}
		if(now-this.lastCastTime>=this.cd*1000){
			$('#record').append('<p>'+now.Format('hh:mm:ss')+'<font color="red">使用乱舞</font>'+'</p>');
			isBladeFlurry = true;
			scrollBottom();
			console.log(mainHandSpeed+","+offHandSpeed)
			if(issliceAndDice){
				mainHandSpeed = 1/(1.3*1.2/attr.mainHandSpeed);
				offHandSpeed = 1/(1.3*1.2/attr.offHandSpeed);
			}else{
				mainHandSpeed = 1/(1.2/attr.mainHandSpeed);
				offHandSpeed = 1/(1.2/attr.offHandSpeed);
			}
			console.log("主手速度："+mainHandSpeed+",副手速度"+offHandSpeed)
			this.lastCastTime= now;
			lastSkillTime = now;
			this.paint();
			setTimeout(function(){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'<font color="red">乱舞结束</font>'+'</p>');
				scrollBottom();
				isBladeFlurry = false; 
				if(issliceAndDice){
					mainHandSpeed = 1/(1.3/attr.mainHandSpeed);
					offHandSpeed = 1/(1.3/attr.offHandSpeed);
				}else{
					mainHandSpeed = attr.mainHandSpeed;
					offHandSpeed = attr.offHandSpeed;
				}
				console.log("主手速度："+mainHandSpeed+",副手速度"+offHandSpeed)
			},15*1000);
		}
	}
	paint(){
		 if(this.active)  
	            return; 
		 var canvas = document.getElementById('bladeFlurryCanvas');
		 var context = canvas.getContext("2d");  
	     var canvasWidth = canvas.width,  
	      canvasHeight = canvas.height,  
	      cx = canvasWidth/2,  
	      cy = canvasHeight/2;     
		     
		     //画一个半透明的灰色背景  
	      context.beginPath();  
	      context.clearRect(0,0,canvasWidth,canvasHeight);  
	      context.moveTo(0,0);  
	      context.fillStyle = 'rgba(0,0,0,.7)';  
	      context.fillRect(0,0,canvasWidth,canvasHeight);  
	      context.fill();  
	      
	      var speed = 5*1000,  
	      step = 2*Math.PI/24,  
	      start = 3*Math.PI/2,  
	      begin = start,  
	      end = start + step,  
	      len = canvas.width > canvas.height ? canvas.width:canvas.height,  
	      r = Math.round(0.8*len);  
		     if(canvas.getAttribute('sec')){  
		         speed = Math.round(50*canvas.getAttribute('sec')/9);  
		     }  
		     var _this = this;
		     context.beginPath();  
		        context.moveTo(cx, cy);  
		        context.fillStyle = 'rgb(0,0,0)';  
		        context.globalCompositeOperation = "destination-out";  
		        var that = $('#bladeFlurryCanvas');;  
		  
		        var timer = null;  
		        var doPaint = function(){  
		            context.arc(cx, cy, r, start, end, false);  
		            context.fill();  
		            start = end;  
		            end = end + step;  
		            if(end - begin > 2*Math.PI){  
		                clearTimeout(timer);  
		                context.globalCompositeOperation = "source-over";  
		                _this.active = false;  
		                return;  
		            }   
		            else  
		               timer = setTimeout(doPaint,speed);  
		              
		        }  
		        doPaint();  
	}
}
class SpecialEffects{
	trigger(){
		this.InstantPoisonVI();
		mainHand.swordEffect();
	}
	InstantPoisonVI(){
		var random = Math.random();
		if(random<=0.2){
			var r=0;
			random== Math.random();
			if(random<0.05){
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'触发一次毒药，被抵抗了'+'</p>');
			}else{
				r=randomNum(112,149);
				$('#record').append('<p>'+now.Format('hh:mm:ss')+'触发一次毒药，造成'+r+'点自然伤害</p>');
			}
			data[2].total+=r;
			data[2].times+=1;
		}
	}
	ruthless(){
		var random = Math.random();
		if(random<=0.6){
			showStar(1);
			$('#record').append('<p>'+now.Format('hh:mm:ss')+'触发无情奖励一个连击点数</p>');
		}
	}
	relentlessStrikes(p){
		var random = Math.random();
		if(random*100<=p*20){
			$('#record').append('<p>'+now.Format('hh:mm:ss')+'触发无情打击奖励25点能量</p>');
			showEnergy(25);
		}
	}
	
}

function initDatagrid(){
	 data=[{
		'name':'主手攻击',
		'times':0,
		'total':0,
		'crit':0,
		'critTimes':0,
		'miss':0,
		'missTimes':0,
		'dodge':0,
		'dodgeTimes':0
		
	},{
		'name':'副手攻击',
		'times':0,
		'total':0,
		'crit':0,
		'critTimes':0,
		'miss':0,
		'missTimes':0,
		'dodge':0,
		'dodgeTimes':0,
	},{
		'name':'速效毒药',
		'times':0,
		'total':0,
	}];
	
	if(attr.mainHandWeaponType=='剑'){
		data.push({
			'name':'邪恶打击',
			'times':0,
			'total':0,
			'crit':0,
			'critTimes':0,
			'miss':0,
			'missTimes':0,
			'dodge':0,
			'dodgeTimes':0,
		});	
		data.push({
			'name':'剔骨',
			'times':0,
			'total':0,
			'crit':0,
			'critTimes':0,
			'miss':0,
			'missTimes':0,
			'dodge':0,
			'dodgeTimes':0,
		});
		data.push({
			'name':'剑专精',
			'times':0,
			'total':0,
			'crit':0,
			'critTimes':0,
			'miss':0,
			'missTimes':0,
			'dodge':0,
			'dodgeTimes':0,
		});
	}
	
	$('#static').datagrid({
		data:data
	})
	$("#static").datagrid('reload');
}
function reload(){
	var total=0;
	for(var i=0;i<data.length;i++){
		total+=data[i].total;
	}
	for( i=0;i<data.length;i++){
		data[i].percent = parseFloat(data[i].total/total*100).toFixed(2)+"%";
	}
	$('#total').text("总伤害:"+total);
	$('#dps').text("秒伤:"+parseFloat(total/((now-startTime)/1000)).toFixed(2));
	$('#static').datagrid({
		data:data
	})
	$("#static").datagrid('reload');
}

function showEnergy(p){
	energy+=p;
	if(energy>100){
		energy=100;
	}else if(energy<0){
		energy=0;
	}
	$('#energy').text("能量:"+energy);
}
function showStar(p){
	star+=p;
	if(star>5){
		star=5;
	}
	$('#star').text("星:"+star);
}
function randomNum(minNum, maxNum) {
	  switch (arguments.length) {
	    case 1:
	      return parseInt(Math.random() * minNum + 1, 10);
	      break;
	    case 2:
	      return parseInt(Math.random() * ( maxNum - minNum + 1 ) + minNum, 10);
	      //或者 Math.floor(Math.random()*( maxNum - minNum + 1 ) + minNum );
	      break;
	    default:
	      return 0;
	      break;
	  }
	}
function fomatFloat(num,n){   
  var f = parseFloat(num);
  if(isNaN(f)){
      return false;
  }   
  f = Math.round(num*Math.pow(10, n))/Math.pow(10, n); // n 幂   
  var s = f.toString();
  var rs = s.indexOf('.');
  //判定如果是整数，增加小数点再补0
  if(rs < 0){
      rs = s.length;
      s += '.'; 
  }
  while(s.length <= rs + n){
      s += '0';
  }
  return s;
}  

function scrollBottom(){
	  var scrollHeight = $('#record').prop("scrollHeight");
    $('#record').scrollTop(scrollHeight,200);

}
Date.prototype.Format = function(formatStr)   
{   
    var str = formatStr;   
    var Week = ['日','一','二','三','四','五','六'];  
   
    str=str.replace(/yyyy|YYYY/,this.getFullYear());   
    str=str.replace(/yy|YY/,(this.getYear() % 100)>9?(this.getYear() % 100).toString():'0' + (this.getYear() % 100));   
   
    str=str.replace(/MM/,this.getMonth()>9?this.getMonth().toString():'0' + this.getMonth());   
    str=str.replace(/M/g,this.getMonth());   
   
    str=str.replace(/w|W/g,Week[this.getDay()]);   
   
    str=str.replace(/dd|DD/,this.getDate()>9?this.getDate().toString():'0' + this.getDate());   
    str=str.replace(/d|D/g,this.getDate());   
   
    str=str.replace(/hh|HH/,this.getHours()>9?this.getHours().toString():'0' + this.getHours());   
    str=str.replace(/h|H/g,this.getHours());   
    str=str.replace(/mm/,this.getMinutes()>9?this.getMinutes().toString():'0' + this.getMinutes());   
    str=str.replace(/m/g,this.getMinutes());   
   
    str=str.replace(/ss|SS/,this.getSeconds()>9?this.getSeconds().toString():'0' + this.getSeconds());   
    str=str.replace(/s|S/g,this.getSeconds());   
   
    return str;   
} 

