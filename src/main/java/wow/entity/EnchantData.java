package wow.entity;

import java.io.Serializable;

public class EnchantData implements Serializable {
	
	private int strength;
	private int nimble;
	private int intelligence;
	private int mental;
	private int endurance;
	private int ap;
	
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	public int getNimble() {
		return nimble;
	}
	public void setNimble(int nimble) {
		this.nimble = nimble;
	}
	public int getIntelligence() {
		return intelligence;
	}
	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}
	public int getMental() {
		return mental;
	}
	public void setMental(int mental) {
		this.mental = mental;
	}
	public int getEndurance() {
		return endurance;
	}
	public void setEndurance(int endurance) {
		this.endurance = endurance;
	}
	public int getAp() {
		return ap;
	}
	public void setAp(int ap) {
		this.ap = ap;
	}
	
	
}
