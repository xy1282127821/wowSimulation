package wow.entity;

public class RequestData {
	private String race;
	private String[]equipments;
	private String[] buffs;
	private EnchantData enchantData;
	private String mainHand;
	private String offHand;
	
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public String[] getEquipments() {
		return equipments;
	}
	public void setEquipments(String[] equipments) {
		this.equipments = equipments;
	}
	public String[] getBuffs() {
		return buffs;
	}
	public void setBuffs(String[] buffs) {
		this.buffs = buffs;
	}
	public EnchantData getEnchantData() {
		return enchantData;
	}
	public void setEnchantData(EnchantData enchantData) {
		this.enchantData = enchantData;
	}
	public String getMainHand() {
		return mainHand;
	}
	public void setMainHand(String mainHand) {
		this.mainHand = mainHand;
	}
	public String getOffHand() {
		return offHand;
	}
	public void setOffHand(String offHand) {
		this.offHand = offHand;
	}
	
	
}
