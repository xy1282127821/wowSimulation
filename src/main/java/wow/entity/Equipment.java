package wow.entity;

public class Equipment {
	private Integer id;
	private String equipName;
	private String equipPostion;
	private String equipType;
	private String equipDamage;
	private String equipSpeed;
	private String equipAttr;
	private String equipEffect;
	private String equipImg;
	private String equipId;
	private String equipTooltip;
	public Integer getId() {
		return id;
	}
	public String getEquipName() {
		return equipName;
	}
	public void setEquipName(String equipName) {
		this.equipName = equipName;
	}
	public String getEquipPostion() {
		return equipPostion;
	}
	public void setEquipPostion(String equipPostion) {
		this.equipPostion = equipPostion;
	}
	public String getEquipType() {
		return equipType;
	}
	public void setEquipType(String equipType) {
		this.equipType = equipType;
	}
	public String getEquipDamage() {
		return equipDamage;
	}
	public void setEquipDamage(String equipDamage) {
		this.equipDamage = equipDamage;
	}
	public String getEquipSpeed() {
		return equipSpeed;
	}
	public void setEquipSpeed(String equipSpeed) {
		this.equipSpeed = equipSpeed;
	}
	public String getEquipAttr() {
		return equipAttr;
	}
	public void setEquipAttr(String equipAttr) {
		this.equipAttr = equipAttr;
	}
	public String getEquipEffect() {
		return equipEffect;
	}
	public void setEquipEffect(String equipEffect) {
		this.equipEffect = equipEffect;
	}
	public String getEquipImg() {
		return equipImg;
	}
	public void setEquipImg(String equipImg) {
		this.equipImg = equipImg;
	}
	public String getEquipId() {
		return equipId;
	}
	public void setEquipId(String equipId) {
		this.equipId = equipId;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEquipTooltip() {
		return equipTooltip;
	}
	public void setEquipTooltip(String equipTooltip) {
		this.equipTooltip = equipTooltip;
	}
	
	
}	
