package wow.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wow.dao.BuffDao;

@Service
public class BuffService {
	@Autowired
	private BuffDao dao;
	
	public List<HashMap> findAll(){
		return dao.findAll();
	}
}
