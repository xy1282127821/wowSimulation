package wow.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import wow.dao.EquipmentDao;
import wow.entity.Equipment;

@Service
public class EquipmentService {
	@Autowired
	private EquipmentDao dao;
	
	public void insert(Equipment entity) {
		dao.insert(entity);
	}
	
	public List<HashMap> findAll(){
		return dao.findAll();
	}
	@SuppressWarnings(value = { "all" })
	public PageInfo<HashMap> getEquipList(int page, int rows,String type, String position,String euipName) {
		HashMap searchParams = new HashMap();
		if (position.equals("helmet")) {
			searchParams.put("equipPostion", "头部");
		}else if (position.equals("shoulder")) {
			searchParams.put("equipPostion", "肩部");
		}else if (position.equals("breastplate")) {
			searchParams.put("equipPostion", "胸部");
		}else if(position.equals("bracer")) {
			searchParams.put("equipPostion", "手腕");
		}else if (position.equals("gauntlets")) {
			searchParams.put("equipPostion", "手");
		}else if (position.equals("belt")) {
			searchParams.put("equipPostion", "腰部");
		}else if (position.equals("pants")) {
			searchParams.put("equipPostion", "腿部");
		}else if (position.equals("boots")) {
			searchParams.put("equipPostion", "脚");
		}
		if(type.equals("leather")) {
			searchParams.put("equipType", "皮甲");
		}else if(type.equals("chain")) {
			searchParams.put("equipType", "锁甲");
		}else if(type.equals("plate")) {
			searchParams.put("equipType", "板甲");
		}else if(type.equals("cloth")) {
			searchParams.put("equipType", "布甲");
		}
		if (position.equals("cloak")) {
			searchParams.put("equipPostion", "背部");
			searchParams.put("equipType", "布甲");
		}else if (position.equals("necklace")) {
			searchParams.put("equipPostion", "颈部");
			searchParams.put("equipType", "");
		}else if (position.equals("ring1")||position.equals("ring2")) {
			searchParams.put("equipPostion", "戒指");
			searchParams.put("equipType", "");
		}else if (position.equals("talisman1")||position.equals("talisman2")) {
			searchParams.put("equipPostion", "饰品");
			searchParams.put("equipType", "");
		}
		if(position.equals("mainHand")) {
			if(type.equals("leather")) {
				searchParams.put("equipType", "");
				searchParams.put("equipPostion", "");
				searchParams.put("mainHand", "#");
			}else {
				searchParams.put("equipType", "");
				searchParams.put("equipPostion", "");
				searchParams.put("warriorMainHand", "#");
			}
			
		}
		if(position.equals("offHand")) {
			if(type.equals("leather")) {
				searchParams.put("equipType", "");
				searchParams.put("equipPostion", "");
				searchParams.put("offHand", "#");
			}else {
				searchParams.put("equipType", "");
				searchParams.put("equipPostion", "");
				searchParams.put("warriorOffHand", "#");
			}
		}
		if(position.equals("bow")) {
			searchParams.put("equipPostion", "远程");
			searchParams.put("equipType", "");
		}
		
		searchParams.put("equipName", euipName);
		
		PageHelper.startPage(page, rows);
		List<HashMap> list=dao.getEquipList(searchParams);
		PageInfo<HashMap> pageInfo = new PageInfo<HashMap>(list);
		return pageInfo;
	}
	
	public Equipment findByCode(String code) {
		return dao.findByCode(code);
	}
}
