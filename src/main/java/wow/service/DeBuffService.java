package wow.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wow.dao.DeBuffDao;
@Service
public class DeBuffService {
	@Autowired
	private DeBuffDao dao;
	
	public List<HashMap> findAll(){
		return dao.findAll();
	}
}
