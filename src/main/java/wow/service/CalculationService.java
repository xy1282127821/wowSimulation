package wow.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.util.StringUtil;

import wow.common.DamageItem;
import wow.dao.BuffDao;
import wow.dao.EquipmentDao;
import wow.entity.EnchantData;
import wow.entity.Equipment;
import wow.entity.RequestData;
import wow.util.rouge.RaceFactory;
import wow.util.rouge.RougeRole;
import wow.util.suit.RogueSuitUtil;
import wow.util.suit.WarriorSuitUtil;
import wow.util.warrior.WarriorFactory;
import wow.util.warrior.WarriorRole;

@Service
public class CalculationService {
	@Autowired
	private EquipmentDao equipmentDao;
	
	/**
	 * 获得盗贼的基础属性
	 * @param requestData
	 * @return
	 * @throws Exception
	 */
	public RougeRole getRogueAttr(RequestData requestData) throws Exception {
		RougeRole role = RaceFactory.getInstence(requestData.getRace());
	    
	    List<Equipment> equipmentList = new ArrayList<Equipment>(19);
	    Equipment mainHandWeapon = null;
	    Equipment offHandWeapon = null;
	    
	    for(String code:requestData.getEquipments()) {
	    	Equipment equipment = equipmentDao.findByCode(code);
	    	if(equipment.getEquipId().equals(requestData.getMainHand())) {
	    		mainHandWeapon=equipment;
			}
	    	if(equipment.getEquipId().equals(requestData.getOffHand())) {
				offHandWeapon=equipment;
			}
	    	if(equipment.getEquipId().equals("11815")&&!role.getSpecial().contains("11815")) {
	    		role.getSpecial().add("11815");
	    	}
	    	if(equipment.getEquipId().equals("19019")) {
	    		if(requestData.getMainHand().equals("19019")&&!role.getSpecial().contains("19019m")) {
	    			role.getSpecial().add("19019m");
	    		}else if(requestData.getOffHand().equals("19019")&&!role.getSpecial().contains("19019o")) {
	    			role.getSpecial().add("19019o");
	    		}
	    	}
	    	equipmentList.add(equipment);
	    }
	    
	    
	    
		for(Equipment equipment:equipmentList) {
			String equipAttrStr =equipment.getEquipAttr();
			//System.out.print(equipment.getEquipName()+":");
			if(null!=equipAttrStr) {
				String[] equipAttrs=equipAttrStr.split(";");
				for(String  equipAttr:equipAttrs) {
					if(equipAttr.contains("力量")) {
						String num=equipAttr.replace("+", "").replaceAll(" ", "").replaceAll("力量", "");
						role.setStrength(role.getStrength()+Integer.parseInt(num));
					}else if(equipAttr.contains("敏捷")) {
						String num=equipAttr.replace("+", "").replaceAll(" ", "").replaceAll("敏捷", "");
						role.setNimble(role.getNimble()+Integer.parseInt(num));
					}else if(equipAttr.contains("耐力")) {
						String num=equipAttr.replace("+", "").replaceAll(" ", "").replaceAll("耐力", "");
						role.setEndurance(role.getEndurance()+Integer.parseInt(num));
					}
				}
			}
			String effectStr = equipment.getEquipEffect();
			if(effectStr!=null) {
				String[]  effects = effectStr.split("。");
				for(String effect:effects) {
					if(effect.contains("装备: 使你造成致命一击的几率提高")) {
						String num=effect.replace("装备: 使你造成致命一击的几率提高", "").replaceAll("%", "");
						role.setCrit(role.getCrit()+Float.parseFloat(num));
					}else if(effect.contains("装备: 使你击中目标的几率提高")) {
						String num=effect.replace("装备: 使你击中目标的几率提高", "").replaceAll("%", "");
						role.setHitRate(role.getHitRate()+Float.parseFloat(num));
					}else if(effect.contains("装备: +")&&effect.contains("攻击强度")) {
						String num=effect.replaceAll("装备: \\+", "").replaceAll(" 攻击强度", "").trim();
						role.setAp(role.getAp()+Integer.parseInt(num));
					}else if(effect.contains("技能提高")) {
						if(effect.contains("剑类武器")) {
							String num=effect.replaceAll("装备: 剑类武器技能提高", "").replaceAll("点", "").trim();
							if(mainHandWeapon.getEquipType().equals("剑")) {
								role.setMainHandSkill(role.getMainHandSkill()+Integer.parseInt(num));
							}
							if(offHandWeapon.getEquipType().equals("剑")) {
								role.setOffHandSkill(role.getOffHandSkill()+Integer.parseInt(num));
							}
						}else if(effect.contains("匕首")) {
							String num=effect.replaceAll("装备: 匕首技能提高", "").replaceAll("点", "").trim();
							if(mainHandWeapon.getEquipType().equals("匕首")) {
								role.setMainHandSkill(role.getMainHandSkill()+Integer.parseInt(num));
							}
							if(offHandWeapon.getEquipType().equals("匕首")) {
								role.setOffHandSkill(role.getOffHandSkill()+Integer.parseInt(num));
							}
						}
					}
				}
			}
			
		}
		RogueSuitUtil.suitEffect(role, requestData.getEquipments());
		
		EnchantData enchantData=requestData.getEnchantData();
		role.setStrength(role.getStrength()+enchantData.getStrength());
		role.setNimble(role.getNimble()+enchantData.getNimble());
		role.setEndurance(role.getEndurance()+enchantData.getEndurance());
		role.setAp(role.getAp()+enchantData.getAp());
		
		String[] buffs = requestData.getBuffs();
		for(String buff:buffs) {
			if(buff.equals("buff2")) {
				role.setAp(role.getAp()+155);
			}
			if(buff.equals("buff3")) {
				DecimalFormat decimalFormat=new DecimalFormat("#");
				decimalFormat.format(role.getStrength()*1.1);
				role.setStrength(Integer.parseInt(decimalFormat.format(role.getStrength()*1.1)));
				role.setNimble(Integer.parseInt(decimalFormat.format(role.getNimble()*1.1)));
				role.setEndurance(Integer.parseInt(decimalFormat.format(role.getEndurance()*1.1)));
			}
			if(buff.equals("buff4")) {
				role.setStrength(role.getStrength()+12);
				role.setNimble(role.getNimble()+12);
				role.setEndurance(role.getEndurance()+12);
			}
			if(buff.equals("buff5")) {
				role.setStrength(role.getStrength()+30);
			}
			if(buff.equals("buff6")) {
				role.setNimble(role.getNimble()+25);
				role.setCrit(role.getCrit()+2);
			}
			if(buff.equals("buff7")) {
				role.setAp(role.getAp()+35);
			}
			if(buff.equals("buff8")) {
				role.setCrit(role.getCrit()+5);
				role.setAp(role.getAp()+160);
			}
			if(buff.equals("buff9")) {
				role.setCrit(role.getCrit()+5);
				role.setStrength(role.getStrength()+15);
				role.setNimble(role.getNimble()+15);
				role.setEndurance(role.getEndurance()+15);
			}
			if(buff.equals("buff10")) {
				DecimalFormat decimalFormat=new DecimalFormat("#");
				role.setStrength(Integer.parseInt(decimalFormat.format(role.getStrength()*1.15)));
				role.setNimble(Integer.parseInt(decimalFormat.format(role.getNimble()*1.15)));
				role.setEndurance(Integer.parseInt(decimalFormat.format(role.getEndurance()*1.15)));
			}
			if(buff.equals("buff11")) {
				role.setAp(role.getAp()+241);
			}
			if(buff.equals("buff12")) {
				role.setAp(role.getAp()+200);
			}
			if(buff.equals("buff13")) {
				role.setAp(role.getAp()+100);
			}
			if(buff.equals("buff14")) {
				role.special.add("mc+2");
				role.mainHandCrit+=2;
			}
			if(buff.equals("buff15")) {
				role.special.add("oc+2");
				role.offHandCrit+=2;
			}
			if(buff.equals("buff16")) {
				role.special.add("m+8");
			}
			if(buff.equals("buff17")) {
				role.special.add("o+8");
			}
			if(buff.equals("buff18")) {
				role.crit+=3;
			}
		}
	
		role.getAttr(mainHandWeapon,offHandWeapon);
		return role;
	}
	
	
	public WarriorRole getWarriorAttr(RequestData requestData) throws Exception{
		WarriorRole role = WarriorFactory.getInstence(requestData.getRace());
	    
	    List<Equipment> equipmentList = new ArrayList<Equipment>(19);
	    Equipment mainHandWeapon = null;
	    Equipment offHandWeapon = null;
	    
	    for(String code:requestData.getEquipments()) {
	    	if(StringUtil.isEmpty(code)) {
	    		continue;
	    	}
	    	Equipment equipment = equipmentDao.findByCode(code);
	    	if(equipment.getEquipId().equals(requestData.getMainHand())) {
	    		mainHandWeapon=equipment;
			}
	    	if(equipment.getEquipId().equals(requestData.getOffHand())) {
				offHandWeapon=equipment;
			}
	    	if(equipment.getEquipId().equals("11815")&&!role.getSpecial().contains("11815")) {
	    		role.getSpecial().add("11815");
	    	}
	    	if(equipment.getEquipId().equals("19019")) {
	    		if(requestData.getMainHand().equals("19019")&&!role.getSpecial().contains("19019m")) {
	    			role.getSpecial().add("19019m");
	    		}else if(requestData.getOffHand().equals("19019")&&!role.getSpecial().contains("19019o")) {
	    			role.getSpecial().add("19019o");
	    		}
	    	}
	    	if(equipment.getEquipId().equals("871")) {
	    		if(requestData.getMainHand().equals("871")&&!role.getSpecial().contains("871m")) {
	    			role.getSpecial().add("871m");
	    		}else if(requestData.getOffHand().equals("871")&&!role.getSpecial().contains("871o")) {
	    			role.getSpecial().add("871o");
	    		}
	    	}
	    	equipmentList.add(equipment);
	    }
	    
	    
	    
		for(Equipment equipment:equipmentList) {
			String equipAttrStr =equipment.getEquipAttr();
			//System.out.print(equipment.getEquipName()+":");
			if(null!=equipAttrStr) {
				String[] equipAttrs=equipAttrStr.split(";");
				for(String  equipAttr:equipAttrs) {
					if(equipAttr.contains("力量")) {
						String num=equipAttr.replace("+", "").replaceAll(" ", "").replaceAll("力量", "");
						role.setStrength(role.getStrength()+Integer.parseInt(num));
					}else if(equipAttr.contains("敏捷")) {
						String num=equipAttr.replace("+", "").replaceAll(" ", "").replaceAll("敏捷", "");
						role.setNimble(role.getNimble()+Integer.parseInt(num));
					}else if(equipAttr.contains("耐力")) {
						String num=equipAttr.replace("+", "").replaceAll(" ", "").replaceAll("耐力", "");
						role.setEndurance(role.getEndurance()+Integer.parseInt(num));
					}
				}
			}
			String effectStr = equipment.getEquipEffect();
			if(effectStr!=null) {
				String[]  effects = effectStr.split("。");
				for(String effect:effects) {
					if(effect.contains("装备: 使你造成致命一击的几率提高")) {
						String num=effect.replace("装备: 使你造成致命一击的几率提高", "").replaceAll("%", "");
						role.setCrit(role.getCrit()+Float.parseFloat(num));
					}else if(effect.contains("装备: 使你击中目标的几率提高")) {
						String num=effect.replace("装备: 使你击中目标的几率提高", "").replaceAll("%", "");
						role.setHitRate(role.getHitRate()+Float.parseFloat(num));
					}else if(effect.contains("装备: +")&&effect.contains("攻击强度")) {
						String num=effect.replaceAll("装备: \\+", "").replaceAll(" 攻击强度", "").trim();
						role.setAp(role.getAp()+Integer.parseInt(num));
					}else if(effect.contains("技能提高")) {
						if(effect.contains("剑类武器")) {
							String num=effect.replaceAll("装备: 剑类武器技能提高", "").replaceAll("点", "").trim();
							if(mainHandWeapon.getEquipType().equals("剑")) {
								role.setMainHandSkill(role.getMainHandSkill()+Integer.parseInt(num));
							}
							if(offHandWeapon.getEquipType().equals("剑")) {
								role.setOffHandSkill(role.getOffHandSkill()+Integer.parseInt(num));
							}
						}else if(effect.contains("匕首")) {
							String num=effect.replaceAll("装备: 匕首技能提高", "").replaceAll("点", "").trim();
							if(mainHandWeapon.getEquipType().equals("匕首")) {
								role.setMainHandSkill(role.getMainHandSkill()+Integer.parseInt(num));
							}
							if(offHandWeapon.getEquipType().equals("匕首")) {
								role.setOffHandSkill(role.getOffHandSkill()+Integer.parseInt(num));
							}
						}else if(effect.contains("斧类武器")) {
							String num=effect.replaceAll("装备: 斧类武器技能提高", "").replaceAll("点", "").trim();
							if(mainHandWeapon.getEquipType().equals("斧")) {
								role.setMainHandSkill(role.getMainHandSkill()+Integer.parseInt(num));
							}
							if(offHandWeapon.getEquipType().equals("斧")) {
								role.setOffHandSkill(role.getOffHandSkill()+Integer.parseInt(num));
							}
						}else if(effect.contains("锤类武器")) {
							String num=effect.replaceAll("装备: 锤类武器技能提高", "").replaceAll("点", "").trim();
							if(mainHandWeapon.getEquipType().equals("锤")) {
								role.setMainHandSkill(role.getMainHandSkill()+Integer.parseInt(num));
							}
							if(offHandWeapon.getEquipType().equals("锤")) {
								role.setOffHandSkill(role.getOffHandSkill()+Integer.parseInt(num));
							}
						}
						
					}
				}
			}
			
		}
		WarriorSuitUtil.suitEffect(role, requestData.getEquipments());
		
		EnchantData enchantData=requestData.getEnchantData();
		role.setStrength(role.getStrength()+enchantData.getStrength());
		role.setNimble(role.getNimble()+enchantData.getNimble());
		role.setEndurance(role.getEndurance()+enchantData.getEndurance());
		role.setAp(role.getAp()+enchantData.getAp());
		
		String[] buffs = requestData.getBuffs();
		for(String buff:buffs) {
			if(buff.equals("buff2")) {
				role.setAp(role.getAp()+155);
			}
			if(buff.equals("buff3")) {
				DecimalFormat decimalFormat=new DecimalFormat("#");
				decimalFormat.format(role.getStrength()*1.1);
				role.setStrength(Integer.parseInt(decimalFormat.format(role.getStrength()*1.1)));
				role.setNimble(Integer.parseInt(decimalFormat.format(role.getNimble()*1.1)));
				role.setEndurance(Integer.parseInt(decimalFormat.format(role.getEndurance()*1.1)));
			}
			if(buff.equals("buff4")) {
				role.setStrength(role.getStrength()+12);
				role.setNimble(role.getNimble()+12);
				role.setEndurance(role.getEndurance()+12);
			}
			if(buff.equals("buff5")) {
				role.setStrength(role.getStrength()+30);
			}
			if(buff.equals("buff6")) {
				role.setNimble(role.getNimble()+25);
				role.setCrit(role.getCrit()+2);
			}
			if(buff.equals("buff7")) {
				role.setAp(role.getAp()+35);
			}
			if(buff.equals("buff8")) {
				role.setCrit(role.getCrit()+5);
				role.setAp(role.getAp()+160);
			}
			if(buff.equals("buff9")) {
				role.setCrit(role.getCrit()+5);
				role.setStrength(role.getStrength()+15);
				role.setNimble(role.getNimble()+15);
				role.setEndurance(role.getEndurance()+15);
			}
			if(buff.equals("buff10")) {
				DecimalFormat decimalFormat=new DecimalFormat("#");
				role.setStrength(Integer.parseInt(decimalFormat.format(role.getStrength()*1.15)));
				role.setNimble(Integer.parseInt(decimalFormat.format(role.getNimble()*1.15)));
				role.setEndurance(Integer.parseInt(decimalFormat.format(role.getEndurance()*1.15)));
			}
			if(buff.equals("buff11")) {
				role.setAp(role.getAp()+241);
			}
			if(buff.equals("buff12")) {
				role.setAp(role.getAp()+200);
			}
			if(buff.equals("buff13")) {
				role.setAp(role.getAp()+100);
			}
			if(buff.equals("buff14")) {
				role.mainHandCrit+=2;
			}
			if(buff.equals("buff15")) {
				role.offHandCrit+=2;
			}
			if(buff.equals("buff16")) {
				role.special.add("m+8");
			}
			if(buff.equals("buff17")) {
				role.special.add("o+8");
			}
			if(buff.equals("buff18")) {
				role.crit+=3;
			}
			
		}
	
		role.getAttr(mainHandWeapon,offHandWeapon);
		return role;
	}
	
	public static void main(String[] args) {
		String s ="装备: +20 攻击强度";
		System.out.println(s.replaceAll("装备: \\+", "").replaceAll(" 攻击强度", ""));
	}

	public List<DamageItem> getSimulation(RougeRole race,float ac) {
		List<DamageItem> list = null;
		if(race.getMainHandWeaponType().equals("剑")) {
			list = swordCaculation(race, ac);

		}else {
			list = daggerCaculation(race, ac);
		}
		return list;
	}
	private List<DamageItem> daggerCaculation(RougeRole race, float ac){
		List<DamageItem> list = new ArrayList<DamageItem>();
		int time = 120; //设置时间为120秒；
		int mainHandStrikesTimes = getRogueStrikesTimes(race.getMainHandSpeed(),time); //主手攻击次数
		int offHandStrikesTimes = getRogueStrikesTimes(race.getOffHandSpeed(),time); //副手攻击次数
		float mainHandDodgeRate =  getDodgeRate(race.getMainHandSkill());  //主手被躲闪率
		float offHandDodgeRate =  getDodgeRate(race.getMainHandSkill()); //副手被躲闪率
		if(race.getMainHandSkill()<305) {
			race.setMainHandHitRate(race.getHitRate()-1);
		}else {
			race.setMainHandHitRate(race.getHitRate());
		}
		if(race.getOffHandSkill()<305) {
			race.setOffHandHitRate(race.getHitRate()-1);
		}else {
			race.setOffHandHitRate(race.getHitRate());
		}
//		System.out.println("主手命中率+"+race.getMainHandHitRate());
//		System.out.println("副手命中率+"+race.getOffHandHitRate());
		
		//暴击抑制-3%
		race.setCrit(race.mainHandCrit-3);
		race.mainHandCrit-=3;
		race.offHandCrit-=3;
		float mainHandMissRate =  getMissRate(race.getMainHandSkill())-race.getMainHandHitRate(); //主手miss率
		float offHandMissRate =  getMissRate(race.getOffHandSkill())-race.getOffHandHitRate(); //副手miss率
		float skillMissRate = getSkillMissRate(race.getOffHandSkill())-race.getHitRate();
		if(skillMissRate<0) {
			skillMissRate = 0;
		}
		float skillCrit = race.mainHandCrit*(1-skillMissRate/100-mainHandDodgeRate/100);
		float mainHandDef  = getDef(race.getMainHandSkill());
		float offHandDef  = getDef(race.getOffHandSkill());
		
		float mainHandCritThreshold = 100-40-mainHandMissRate-mainHandDodgeRate;
		float mainHandCrit = race.mainHandCrit;
		if(mainHandCritThreshold<race.getCrit()) {
			mainHandCrit = mainHandCritThreshold;
		}
		
		float offHandCritThreshold = 100-40-offHandMissRate-offHandDodgeRate;
		float offHandCrit = race.offHandCrit;
		if(!race.getOffHandWeaponType().equals("匕首")) {
			offHandCrit -=5;
		}
		if(offHandCritThreshold<race.getCrit()) {
			offHandCrit = offHandCritThreshold;
		}
		
		float mainHandCoefficient = getCommonAttackCoefficient(mainHandCrit, mainHandDef, mainHandDodgeRate, mainHandMissRate);
		float offhandCoefficient = getCommonAttackCoefficient(offHandCrit,offHandDef,offHandDodgeRate,offHandMissRate);
		
//		System.out.println("主手攻击次数："+mainHandStrikesTimes);
//		System.out.println("副手攻击次数："+offHandStrikesTimes);
//		System.out.println("主手躲闪几率："+mainHandDodgeRate);
//		System.out.println("主手躲闪几率："+offHandDodgeRate);
//		System.out.println("主手MISS几率："+mainHandMissRate);
//		System.out.println("副手MISS几率："+offHandMissRate);
//		System.out.println("技能MISS几率："+skillMissRate);
//		System.out.println("技能暴击率："+skillCrit);
//		System.out.println("主手偏斜伤害："+mainHandDef);
//		System.out.println("副手偏斜伤害："+offHandDef);
//		System.out.println("主手伤害系数："+mainHandCoefficient);
//		System.out.println("主手伤害系数："+offhandCoefficient);
		
		DecimalFormat decimalFormat=new DecimalFormat("#.##");
		DecimalFormat decimalFormat2=new DecimalFormat("#");
		
		
		float dr=ac/(ac+85*60+400) ;
		DamageItem mainHandDamage = new DamageItem();
		mainHandDamage.setName("主手伤害");
		mainHandDamage.setCrit(decimalFormat.format(mainHandCrit)+"%");
		mainHandDamage.setTimes(mainHandStrikesTimes+"");
		mainHandDamage.setMissRate(mainHandMissRate+"%");
		mainHandDamage.setDeflection("40%");
		float mdph = race.getMainHandDps()*race.getMainHandSpeed()*(1-dr)*mainHandCoefficient;
		mainHandDamage.setHit(decimalFormat2.format(mdph));
		mainHandDamage.setTotal(decimalFormat2.format(mdph*mainHandStrikesTimes));
		list.add(mainHandDamage);
		
		DamageItem offHandDamage = new DamageItem();
		offHandDamage.setName("副手伤害");
		offHandDamage.setCrit(decimalFormat.format(offHandCrit)+"%");
		offHandDamage.setTimes(offHandStrikesTimes+"");
		offHandDamage.setMissRate(offHandMissRate+"%");
		offHandDamage.setDeflection("40%");
		float odph = race.getOffHandDps()*race.getOffHandSpeed()*offhandCoefficient*(1-dr);
		offHandDamage.setHit(decimalFormat2.format(odph));
		offHandDamage.setTotal(decimalFormat2.format(odph*offHandStrikesTimes));
		list.add(offHandDamage);
		
		float backstabCrit = skillCrit+30;
		float backstabCoefficient = (backstabCrit*2*1.24f/100+(1-skillMissRate/100-mainHandDodgeRate/100-backstabCrit/100))*1.2f;
		String[] mainHandWeaponDamages = race.getMainHandWeaponDamages().split("-");
		float dph = (Float.parseFloat(mainHandWeaponDamages[0])+Float.parseFloat(mainHandWeaponDamages[1]))/2;
		
		int hitTimes = Math.round( mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100) + offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100)
				+ 23*(1-skillMissRate/100-mainHandDodgeRate/100));
		
		DamageItem backstabItem = new DamageItem();
		
		float backstabDamage =((dph+race.getAp()*1.7f/14)*1.5f+210)*backstabCoefficient*(1-dr);
		backstabItem.setName("背刺");
		int backstabTimes =22;
		if(race.getSpecial().contains("RogueT0*6Eeffect")) {
			backstabTimes += (hitTimes*0.02f*35/60);
		}
		backstabItem.setTimes(backstabTimes+"");
		backstabItem.setCrit(decimalFormat.format(backstabCrit)+"%");
		backstabItem.setHit(decimalFormat2.format(backstabDamage));
		backstabItem.setTotal(decimalFormat2.format(backstabTimes*backstabDamage));
		list.add(backstabItem);
		
		
		DamageItem poison = new DamageItem();
		poison.setName("速效毒药");
		int poisonTime =0; 
		if(!race.special.contains("mc+2")&&!race.special.contains("m+8")) {
			poisonTime+=Math.round((36*(1-skillMissRate/100-mainHandDodgeRate/100)+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100))*0.2f);
		}
		if(!race.special.contains("oc+2")&&!race.special.contains("o+8")) {
			poisonTime+=Math.round(offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100)*0.2f);
		}
		poison.setTimes(poisonTime+"");
		poison.setHit(130.5+"");
		poison.setTotal(decimalFormat2.format(poisonTime*130.5*0.95));
		list.add(poison);
		
		for(String code:race.getSpecial()) {
			if(code.equals("11815")) {
				DamageItem handOfJusticeDamage= new DamageItem();
				handOfJusticeDamage.setName("正义之手");
				int handOfJusticeMainHandTimes =Math.round((36*(1-skillMissRate/100-mainHandDodgeRate/100)+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100))*0.02f);
				int handOfJusticeOffHandTimes = Math.round(offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100)*0.02f);
				
				float handOfJusticeDamageTotal = handOfJusticeMainHandTimes*odph+handOfJusticeOffHandTimes*odph;
				handOfJusticeDamage.setTimes(handOfJusticeMainHandTimes+handOfJusticeOffHandTimes+"");
				handOfJusticeDamage.setCrit(decimalFormat.format(mainHandCrit)+"%");
				handOfJusticeDamage.setMissRate(mainHandMissRate+"%");
				handOfJusticeDamage.setTotal(decimalFormat2.format(handOfJusticeDamageTotal));
				list.add(handOfJusticeDamage);
			}else if(code.equals("19019o")) {
				DamageItem windSeekerDamage= new DamageItem();
				windSeekerDamage.setName("风剑特效(副手)");
				windSeekerDamage.setHit("300");
				int windSeekerTimes =Math.round((offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100))*0.2f);
				windSeekerDamage.setTimes(windSeekerTimes+"");
				windSeekerDamage.setTotal(decimalFormat2.format(300*(int)(windSeekerTimes*0.95)));
				list.add(windSeekerDamage);
			}
		}
		
		int tatal=0;
		for(DamageItem item:list) {
			tatal += Integer.parseInt(item.getTotal());
		}
		for(DamageItem item:list) {
			float p = Float.parseFloat(item.getTotal())/tatal*100;
			item.setPercent(decimalFormat.format(p)+"%");
		}
		DamageItem tatalDamage = new DamageItem();
		tatalDamage.setTotal("<strong>"+tatal+"(秒伤："+decimalFormat.format(tatal/120f)+")</strong>");
		tatalDamage.setName("总伤害");
		list.add(0, tatalDamage);
		return list;
	}
	private List<DamageItem> swordCaculation(RougeRole race, float ac) {
		List<DamageItem> list = new ArrayList<DamageItem>();
		int time = 120; //设置时间为120秒；
		int mainHandStrikesTimes = getRogueStrikesTimes(race.getMainHandSpeed(),time); //主手攻击次数
		int offHandStrikesTimes = getRogueStrikesTimes(race.getOffHandSpeed(),time); //副手攻击次数
		float mainHandDodgeRate =  getDodgeRate(race.getMainHandSkill());  //主手被躲闪率
		float offHandDodgeRate =  getDodgeRate(race.getMainHandSkill()); //副手被躲闪率
		if(race.getMainHandSkill()<305) {
			race.setMainHandHitRate(race.getHitRate()-1);
		}else {
			race.setMainHandHitRate(race.getHitRate());
		}
		if(race.getOffHandSkill()<305) {
			race.setOffHandHitRate(race.getHitRate()-1);
		}else {
			race.setOffHandHitRate(race.getHitRate());
		}
//		System.out.println("主手命中率+"+race.getMainHandHitRate());
//		System.out.println("副手命中率+"+race.getOffHandHitRate());
		
		//暴击抑制-3%
		race.setCrit(race.getCrit()-3);
		race.mainHandCrit-=3;
		race.offHandCrit-=3;
		
		float mainHandMissRate =  getMissRate(race.getMainHandSkill())-race.getMainHandHitRate(); //主手miss率
		float offHandMissRate =  getMissRate(race.getOffHandSkill())-race.getOffHandHitRate(); //副手miss率
		float skillMissRate = getSkillMissRate(race.getOffHandSkill())-race.getHitRate();
		if(skillMissRate<0) {
			skillMissRate = 0;
		}
		float skillCrit = race.mainHandCrit*(1-skillMissRate/100-mainHandDodgeRate/100);
		float mainHandDef  = getDef(race.getMainHandSkill());
		float offHandDef  = getDef(race.getOffHandSkill());
		
		float mainHandCritThreshold = 100-40-mainHandMissRate-mainHandDodgeRate;
		float mainHandCrit = race.mainHandCrit;
		if(mainHandCritThreshold<race.mainHandCrit) {
			mainHandCrit = mainHandCritThreshold;
		}
		
		float offHandCritThreshold = 100-40-offHandMissRate-offHandDodgeRate;
		float offHandCrit = race.offHandCrit;
		if(offHandCritThreshold<race.offHandCrit) {
			offHandCrit = offHandCritThreshold;
		}
		
		float mainHandCoefficient = getCommonAttackCoefficient(mainHandCrit, mainHandDef, mainHandDodgeRate, mainHandMissRate);
		float skillCoefficient = getSkillCoefficient(skillCrit,mainHandDodgeRate,skillMissRate);
		float offhandCoefficient = getCommonAttackCoefficient(offHandCrit,offHandDef,offHandDodgeRate,offHandMissRate);
		
//		System.out.println("主手攻击次数："+mainHandStrikesTimes);
//		System.out.println("副手攻击次数："+offHandStrikesTimes);
//		System.out.println("主手躲闪几率："+mainHandDodgeRate);
//		System.out.println("主手躲闪几率："+offHandDodgeRate);
//		System.out.println("主手MISS几率："+mainHandMissRate);
//		System.out.println("副手MISS几率："+offHandMissRate);
//		System.out.println("技能MISS几率："+skillMissRate);
//		System.out.println("技能暴击率："+skillCrit);
//		System.out.println("主手偏斜伤害："+mainHandDef);
//		System.out.println("副手偏斜伤害："+offHandDef);
//		System.out.println("主手伤害系数："+mainHandCoefficient);
//		System.out.println("技能伤害系数："+skillCoefficient);
//		System.out.println("主手伤害系数："+offhandCoefficient);
		
		DecimalFormat decimalFormat=new DecimalFormat("#.##");
		DecimalFormat decimalFormat2=new DecimalFormat("#");
		
		
		float dr=ac/(ac+85*60+400) ;
		DamageItem mainHandDamage = new DamageItem();
		mainHandDamage.setName("主手伤害");
		mainHandDamage.setCrit(decimalFormat.format(mainHandCrit)+"%");
		mainHandDamage.setTimes(mainHandStrikesTimes+"");
		mainHandDamage.setMissRate(mainHandMissRate+"%");
		mainHandDamage.setDeflection("40%");
		float mdph = race.getMainHandDps()*race.getMainHandSpeed()*(1-dr)*mainHandCoefficient;
		mainHandDamage.setHit(decimalFormat2.format(mdph));
		mainHandDamage.setTotal(decimalFormat2.format(mdph*mainHandStrikesTimes));
		list.add(mainHandDamage);
		
		DamageItem offHandDamage = new DamageItem();
		offHandDamage.setName("副手伤害");
		offHandDamage.setCrit(decimalFormat.format(offHandCrit)+"%");
		offHandDamage.setTimes(offHandStrikesTimes+"");
		offHandDamage.setMissRate(offHandMissRate+"%");
		offHandDamage.setDeflection("40%");
		float odph = race.getOffHandDps()*race.getOffHandSpeed()*offhandCoefficient*(1-dr);
		offHandDamage.setHit(decimalFormat2.format(odph));
		offHandDamage.setTotal(decimalFormat2.format(odph*offHandStrikesTimes));
		list.add(offHandDamage);
		
		int hitTimes = Math.round( mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100) + offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100)
				+ 36*(1-skillMissRate/100-mainHandDodgeRate/100));
		
		DamageItem sinisterStrike = new DamageItem();
		sinisterStrike.setName("邪恶打击");
		sinisterStrike.setCrit(decimalFormat.format(skillCrit)+"%");
		int sinisterStrikeTimes = 32;
		if(race.getSpecial().contains("RogueT0*6Eeffect")) {
			sinisterStrikeTimes += (hitTimes*0.02f*35/40);
		}
		sinisterStrike.setTimes(sinisterStrikeTimes+"");
		sinisterStrike.setMissRate(skillMissRate+"%");;
		String[] mainHandWeaponDamages = race.getMainHandWeaponDamages().split("-");
		float dph = (Float.parseFloat(mainHandWeaponDamages[0])+Float.parseFloat(mainHandWeaponDamages[1]))/2;
		float sinisterStrikeDamage = (dph + 2.4f*race.getAp()/14+68)*skillCoefficient*(1-dr);
		sinisterStrike.setTotal(decimalFormat2.format(sinisterStrikeTimes*sinisterStrikeDamage));
		sinisterStrike.setHit(decimalFormat2.format(sinisterStrikeDamage));
		list.add(sinisterStrike);
		
		DamageItem eviscerate = new DamageItem();
		eviscerate.setName("剔骨");
		eviscerate.setCrit(decimalFormat.format(skillCrit)+"%");
		eviscerate.setTimes(4+"");
		eviscerate.setMissRate(skillMissRate+"%");;

//		float eviscerateDamage = (958+0.15f*race.getAp())*skillCoefficient*(1-dr);
		float eviscerateDamage = (851+0.15f*race.getAp())*skillCoefficient*(1-dr);
		eviscerate.setHit(decimalFormat2.format(eviscerateDamage));
		eviscerate.setTotal(decimalFormat2.format(4*eviscerateDamage));
		list.add(eviscerate);
		
		
		DamageItem poison = new DamageItem();
		poison.setName("速效毒药");
		
		int poisonTime =0;
		if(!race.special.contains("mc+2")&&!race.special.contains("m+8")) {
			poisonTime+=Math.round((36*(1-skillMissRate/100-mainHandDodgeRate/100)+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100))*0.2f);
		}
		if(!race.special.contains("oc+2")&&!race.special.contains("o+8")) {
			poisonTime+=Math.round(offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100)*0.2f);
		}
		
		
		poison.setTimes(poisonTime+"");
		poison.setHit(130.5+"");
		poison.setTotal(decimalFormat2.format(poisonTime*130.5*0.95));
		list.add(poison);
		
		DamageItem swordDamage= new DamageItem();
		swordDamage.setName("剑类专精");
		int swordTimes =0;
		if(race.getOffHandWeaponType().equals("剑")) {
			swordTimes = Math.round(hitTimes*0.05f);
		}else {
			swordTimes = Math.round((36*(1-skillMissRate/100-mainHandDodgeRate/100)+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100))*0.05f);
		}
		swordDamage.setCrit(decimalFormat.format(mainHandCrit)+"%");
		swordDamage.setTimes(swordTimes+"");
		swordDamage.setMissRate(mainHandMissRate+"%");
		swordDamage.setHit(decimalFormat2.format(mdph));
		swordDamage.setTotal(decimalFormat2.format(swordTimes*mdph));
		list.add(swordDamage);
		
		for(String code:race.getSpecial()) {
			if(code.equals("11815")) {
				DamageItem handOfJusticeDamage= new DamageItem();
				handOfJusticeDamage.setName("正义之手");
				int handOfJusticeMainHandTimes =Math.round((36*(1-skillMissRate/100-mainHandDodgeRate/100)+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100))*0.02f);
				int handOfJusticeOffHandTimes = Math.round(offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100)*0.02f);
				float handOfJusticeDamageTotal = handOfJusticeMainHandTimes*odph+handOfJusticeOffHandTimes*odph;
				handOfJusticeDamage.setTimes(handOfJusticeMainHandTimes+handOfJusticeOffHandTimes+"");
				handOfJusticeDamage.setCrit(decimalFormat.format(mainHandCrit)+"%");
				handOfJusticeDamage.setMissRate(mainHandMissRate+"%");
				handOfJusticeDamage.setTotal(decimalFormat2.format(handOfJusticeDamageTotal));
				list.add(handOfJusticeDamage);
			}else if(code.equals("19019m")) {
				DamageItem windSeekerDamage= new DamageItem();
				windSeekerDamage.setName("风剑特效(主手)");
				windSeekerDamage.setHit("300");
				int windSeekerTimes =Math.round((36*(1-skillMissRate/100-mainHandDodgeRate/100)+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100))*0.2f);
				windSeekerDamage.setTimes(windSeekerTimes+"");
				windSeekerDamage.setTotal(decimalFormat2.format(300*(int)(windSeekerTimes*0.95)));
				list.add(windSeekerDamage);
			}else if(code.equals("19019o")) {
				DamageItem windSeekerDamage= new DamageItem();
				windSeekerDamage.setName("风剑特效(副手)");
				windSeekerDamage.setHit("300");
				int windSeekerTimes =Math.round((offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100))*0.2f);
				windSeekerDamage.setTimes(windSeekerTimes+"");
				windSeekerDamage.setTotal(decimalFormat2.format(300*(int)(windSeekerTimes*0.95)));
				list.add(windSeekerDamage);
			}
		}
		
		int tatal=0;
		for(DamageItem item:list) {
			tatal += Integer.parseInt(item.getTotal());
		}
		for(DamageItem item:list) {
			float p = Float.parseFloat(item.getTotal())/tatal*100;
			item.setPercent(decimalFormat.format(p)+"%");
		}
		DamageItem tatalDamage = new DamageItem();
		tatalDamage.setTotal("<strong>"+tatal+"(秒伤："+decimalFormat.format(tatal/120f)+")</strong>");
		tatalDamage.setName("总伤害");
		list.add(0, tatalDamage);
		return list;
	}
	private int getRogueStrikesTimes(float speed,int time) {
		int result = (int) (1.3/speed*(time-15)+1.3*1.2*15/speed);
		return result;
	}
	private float getDef(int skills) {
		float def=0f;
		if(skills<308) {
			def  = (float) ((1.2-0.03*(315-skills)+1.3-0.05*(315-skills))/2);
		}else {
			def=0.95f;
		}
		return def;
	}
	
	private float getCommonAttackCoefficient(float crit,float def,float dodge,float miss) {
		float result =  (float) (crit/100*2+ 0.4*def+(1-crit/100-0.4-dodge/100-miss/100));
		return result;
	}
	private float getSkillCoefficient(float crit,float dodge,float miss) {
		float result = (float) (crit*2*1.3/100+(1-crit/100-dodge/100-miss/100));
		return result;
	}
	private float getDodgeRate(int skills) {
		return (float) (6.5 - 0.1*(skills-300));
	}
	private float getMissRate(int skills) {
		float miss=0f;
		if(skills<305) {
			miss =  (float) (27-0.2*(skills-300));
		}else if(skills>= 305){
		    miss =  (float) (27-2-0.1*(skills-305));
		}
		return miss;
	}
	private float getSkillMissRate(int skills) {
		float miss=0f;
		if(skills<305) {
			miss =  (float) (8-0.2*(skills-300));
		}else if(skills>= 305){
		    miss =  (float) (8-2-0.1*(skills-305));
		}
		return miss;
	}


	public List<DamageItem> getWarriorSimulation(WarriorRole role, int ac) {
		
		List<DamageItem> list = new ArrayList<DamageItem>();
		int time = 120; //设置时间为120秒；
		int mainHandStrikesTimes = (int) (1.3*time/role.getMainHandSpeed()); //主手攻击次数
		int offHandStrikesTimes = (int) (1.3*time/role.getOffHandSpeed()); //副手攻击次数
		float mainHandDodgeRate =  getDodgeRate(role.getMainHandSkill());  //主手被躲闪率
		float offHandDodgeRate =  getDodgeRate(role.getMainHandSkill()); //副手被躲闪率
		if(role.getMainHandSkill()<305) {
			role.setMainHandHitRate(role.getHitRate()-1);
		}else {
			role.setMainHandHitRate(role.getHitRate());
		}
		if(role.getOffHandSkill()<305) {
			role.setOffHandHitRate(role.getHitRate()-1);
		}else {
			role.setOffHandHitRate(role.getHitRate());
		}
//		System.out.println("主手命中率+"+role.getMainHandHitRate());
//		System.out.println("副手命中率+"+role.getOffHandHitRate());
		
		//暴击抑制-3%
		role.setCrit(role.getCrit()-3);
		//狂暴姿态
		role.setCrit(role.getCrit()+3);
		
		float mainHandMissRate = 0;
		if(role.getOffHandDps()==0) {
			mainHandMissRate=getSkillMissRate(role.getMainHandSkill())-role.getHitRate();
			mainHandMissRate=mainHandMissRate>0?mainHandMissRate:0;
		}else {
			mainHandMissRate=getMissRate(role.getMainHandSkill())-role.getMainHandHitRate(); //主手miss率

		}
		float offHandMissRate =  getMissRate(role.getOffHandSkill())-role.getOffHandHitRate(); //副手miss率
		float skillMissRate = getSkillMissRate(role.getOffHandSkill())-role.getHitRate();
		if(skillMissRate<0) {
			skillMissRate = 0;
		}
		float skillCrit = role.getCrit()*(1-skillMissRate/100-mainHandDodgeRate/100);
		float mainHandDef  = getDef(role.getMainHandSkill());
		float offHandDef  = getDef(role.getOffHandSkill());
		
		float mainHandCritThreshold = 100-40-mainHandMissRate-mainHandDodgeRate;
		float mainHandCrit = role.mainHandCrit;
		if(mainHandCritThreshold<role.mainHandCrit) {
			mainHandCrit = mainHandCritThreshold;
		}
		
		float offHandCritThreshold = 100-40-offHandMissRate-offHandDodgeRate;
		float offHandCrit = role.offHandCrit;
		if(offHandCritThreshold<role.offHandCrit) {
			offHandCrit = offHandCritThreshold;
		}
		
		float mainHandCoefficient = getCommonAttackCoefficient(mainHandCrit, mainHandDef, mainHandDodgeRate, mainHandMissRate);
		float offhandCoefficient = getCommonAttackCoefficient(offHandCrit,offHandDef,offHandDodgeRate,offHandMissRate);
		float offHandHeroStrikesCoefficient = getCommonAttackCoefficient(mainHandCrit, mainHandDef, mainHandDodgeRate, skillMissRate);
		float skillCoefficient= getWarriorSkillCoefficient(skillCrit,mainHandDodgeRate , skillMissRate);
	
		float dr=ac/(ac+85*60+400f) ;
		float mdph = role.getMainHandDps()*role.getMainHandSpeed()*(1-dr)*mainHandCoefficient;
		float odph = role.getOffHandDps()*role.getOffHandSpeed()*offhandCoefficient*(1-dr);

//		int x =(int)((mainHandStrikesTimes*mdph/30+offHandStrikesTimes*odph/30
//				+mainHandStrikesTimes*(1-mainHandMissRate-mainHandMissRate)*0.4
//				+offHandStrikesTimes*(1-offHandMissRate-offHandMissRate)*0.4
//				+32*0.4*(1-mainHandMissRate-mainHandMissRate)-30*20-25*12)/(12+mainHandCoefficient*mainHandStrikesTimes*mdph/30
//						+offHandStrikesTimes*offHandStrikesTimes*odph/30
//						-offHandHeroStrikesCoefficient*offHandStrikesTimes*odph/30));
		
//		int x = 0;
		int x =0;
		if(role.getOffHandDps()==0) {
			x=(int) ((mainHandStrikesTimes*mdph/30+
					+31*0.4*(1-mainHandMissRate/100-mainHandMissRate/100)+40
					+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandMissRate/100)*0.4
					-20*30-11*25)/(12+mdph*offhandCoefficient/30));
		}else {
			x=(int) ((mainHandStrikesTimes*mdph/30+offHandStrikesTimes*odph/30+120/3
					+31*0.4*(1-mainHandMissRate/100-mainHandMissRate/100)+40
					+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandMissRate/100)*0.4
					+offHandStrikesTimes*(1-offHandMissRate/100-offHandMissRate/100)*0.4-20*30-11*25)/(12+mdph*offhandCoefficient/30));
		}
		
		int cycloneTime=11;
		int bloodthirstyTime=20;
		if(x<0) {
			int a=x*12;
			cycloneTime+=Math.round(a/25f);
			x=0;
			if(cycloneTime<0) {
				bloodthirstyTime+=Math.round(cycloneTime*25/30f);
				cycloneTime=0;
			}
		}
		
		DecimalFormat decimalFormat=new DecimalFormat("#.##");
		DecimalFormat decimalFormat2=new DecimalFormat("#");
		
		
		
		
	
		DamageItem mainHandDamage = new DamageItem();
		mainHandDamage.setName("主手伤害");
		mainHandDamage.setCrit(decimalFormat.format(mainHandCrit)+"%");
		mainHandDamage.setTimes((mainHandStrikesTimes-x)+"");
		mainHandDamage.setMissRate(mainHandMissRate+"%");
		mainHandDamage.setDeflection("40%");
		mainHandDamage.setHit(decimalFormat2.format(mdph));
		mainHandDamage.setTotal(decimalFormat2.format(mdph*(mainHandStrikesTimes-x)));
		list.add(mainHandDamage);
		
		if(role.getOffHandDps()!=0) {
			DamageItem offHandDamage = new DamageItem();
			offHandDamage.setName("副手伤害");
			offHandDamage.setCrit(decimalFormat.format(offHandCrit)+"%");
			offHandDamage.setTimes(offHandStrikesTimes+"");
			offHandDamage.setMissRate(offHandMissRate+"%");
			offHandDamage.setDeflection("40%");
			offHandDamage.setHit(decimalFormat2.format(odph));
			offHandDamage.setTotal(decimalFormat2.format(odph*offHandStrikesTimes));
			list.add(offHandDamage);
		}
	
		
		DamageItem heroStrikesDamage = new DamageItem();
		heroStrikesDamage.setName("英勇打击");
		heroStrikesDamage.setCrit(decimalFormat.format(skillCrit)+"%");
		heroStrikesDamage.setTimes(x+"");
		float heroStrikesHit = (mdph+157)*skillCoefficient*(1-dr);
		heroStrikesDamage.setHit(decimalFormat2.format(heroStrikesHit));
		heroStrikesDamage.setTotal(decimalFormat2.format(heroStrikesHit*x));
		list.add(heroStrikesDamage);
		
		DamageItem bloodthirstyDamage = new DamageItem();
		bloodthirstyDamage.setName("嗜血");
		bloodthirstyDamage.setCrit(decimalFormat.format(skillCrit)+"%");
		bloodthirstyDamage.setTimes(bloodthirstyTime+"");
		float bloodthirstyHit = role.ap*0.45f*skillCoefficient*(1-dr);
		bloodthirstyDamage.setHit(decimalFormat2.format(bloodthirstyHit));
		bloodthirstyDamage.setTotal(decimalFormat2.format(bloodthirstyHit*bloodthirstyTime));
		list.add(bloodthirstyDamage);
		
		DamageItem cycloneDamage = new DamageItem();
		cycloneDamage.setName("旋风斩");
		cycloneDamage.setCrit(decimalFormat.format(skillCrit)+"%");
		cycloneDamage.setTimes(cycloneTime+"");
		float cycloneHit = mdph*skillCoefficient*(1-dr);
		cycloneDamage.setHit(decimalFormat2.format(cycloneHit));
		cycloneDamage.setTotal(decimalFormat2.format(cycloneHit*cycloneTime));
		list.add(cycloneDamage);
		
		for(String code:role.getSpecial()) {
			if(code.equals("11815")) {
				DamageItem handOfJusticeDamage= new DamageItem();
				handOfJusticeDamage.setName("正义之手");
				int handOfJusticeMainHandTimes =Math.round((36*(1-skillMissRate/100-mainHandDodgeRate/100)+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100))*0.02f);
				int handOfJusticeOffHandTimes =0;
				float handOfJusticeDamageTotal=0;
				if(role.getOffHandDps()!=0) {
					handOfJusticeOffHandTimes= Math.round(offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100)*0.02f);
					handOfJusticeDamageTotal = handOfJusticeMainHandTimes*odph+handOfJusticeOffHandTimes*odph;
				}else {
					handOfJusticeDamageTotal = handOfJusticeMainHandTimes*mdph;
				}
				
				handOfJusticeDamage.setTimes(handOfJusticeMainHandTimes+handOfJusticeOffHandTimes+"");
				handOfJusticeDamage.setCrit(decimalFormat.format(mainHandCrit)+"%");
				handOfJusticeDamage.setMissRate(mainHandMissRate+"%");
				handOfJusticeDamage.setTotal(decimalFormat2.format(handOfJusticeDamageTotal));
				list.add(handOfJusticeDamage);
			}else if(code.equals("19019m")) {
				DamageItem windSeekerDamage= new DamageItem();
				windSeekerDamage.setName("风剑特效(主手)");
				windSeekerDamage.setHit("300");
				int windSeekerTimes =Math.round((36*(1-skillMissRate/100-mainHandDodgeRate/100)+mainHandStrikesTimes*(1-mainHandMissRate/100-mainHandDodgeRate/100))*0.2f);
				windSeekerDamage.setTimes(windSeekerTimes+"");
				windSeekerDamage.setTotal(decimalFormat2.format(300*(int)(windSeekerTimes*0.95)));
				list.add(windSeekerDamage);
			}else if(code.equals("19019o")) {
				DamageItem windSeekerDamage= new DamageItem();
				windSeekerDamage.setName("风剑特效(副手)");
				windSeekerDamage.setHit("300");
				int windSeekerTimes =Math.round((offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100))*0.2f);
				windSeekerDamage.setTimes(windSeekerTimes+"");
				windSeekerDamage.setTotal(decimalFormat2.format(300*(int)(windSeekerTimes*0.95)));
				list.add(windSeekerDamage);
			}
//			else if(code.equals("871o")) {
//				DamageItem flurryDamage= new DamageItem();
//				flurryDamage.setName("风暴战斧");
//				flurryDamage.setHit(odph+"");
//				int windSeekerTimes =Math.round((offHandStrikesTimes*(1-offHandMissRate/100-offHandDodgeRate/100))*0.05f);
//				flurryDamage.setTimes(windSeekerTimes+"");
//				flurryDamage.setTotal(decimalFormat2.format(odph*windSeekerTimes));
//				list.add(flurryDamage);
//			}
		}
		int tatal=0;
		for(DamageItem item:list) {
			tatal += Integer.parseInt(item.getTotal());
		}
		for(DamageItem item:list) {
			float p = Float.parseFloat(item.getTotal())/tatal*100;
			item.setPercent(decimalFormat.format(p)+"%");
		}
		DamageItem tatalDamage = new DamageItem();
		tatalDamage.setTotal("<strong>"+tatal+"(秒伤："+decimalFormat.format(tatal/120f)+")</strong>");
		tatalDamage.setName("总伤害");
		list.add(0, tatalDamage);
		return list;
	}
	private float getWarriorSkillCoefficient(float crit,float dodge,float miss) {
		float result = (float) (crit*2*1.2/100+(1-crit/100-dodge/100-miss/100));
		return result;
	}
	
	
}
