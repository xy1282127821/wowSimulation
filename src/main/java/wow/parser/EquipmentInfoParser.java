package wow.parser;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;
import wow.common.HttpClientUtil;
import wow.entity.Equipment;
import wow.service.EquipmentService;

public class EquipmentInfoParser implements PageProcessor{
	private EquipmentService service;
	private String imgUrl;
	private String id;
	
	
	
	public EquipmentInfoParser(EquipmentService service,String imgUrl,String id) {
		super();
		this.service = service;
		this.imgUrl = imgUrl;
		this.id = id;
	}


	private Site site = Site.me().setRetryTimes(3).setSleepTime(1000);
	public Site getSite() {
		return site;
	}

	public void process(Page page) {
		
		Equipment e=  new Equipment();
		e.setEquipId(id);
	
    	Selectable s=page.getHtml().xpath("//div[@class='tooltip']");
    	Selectable b= s.xpath("//b/text()");
    	System.out.println("name:"+b.toString());
    	e.setEquipName(b.toString());
        List<Selectable> nodes = s.xpath("//table[@width='100%']").nodes();
        for(int i = 0;i<nodes.size();i++) {
        	Selectable n = nodes.get(i);
        	if(i==0) {
        		System.out.println("position:"+n.xpath("//td/text()"));
            	System.out.println("type:"+n.xpath("//th/text()"));
            	e.setEquipPostion(n.xpath("//td/text()").toString());
            	e.setEquipType(n.xpath("//th/text()").toString());
        	}else {
        		System.out.println("damage:"+n.xpath("//td/text()"));
            	System.out.println("speed:"+n.xpath("//th/text()"));
            	e.setEquipDamage(n.xpath("//td/text()").toString());
            	e.setEquipSpeed(n.xpath("//th/text()").toString());
        	}
        }
        String str = s.toString();
//        System.out.println(str);
        Pattern r = Pattern.compile("</table>.*?<br></td>");
        Matcher m = r.matcher(str);
       	if(m.find()) {
       	 	str = m.group().replaceAll("</table>", "").replaceAll("</td>", "").replaceAll("<br>", ";").replaceAll("<[^>]+>", "");
       	 	System.out.println(str);
       	 	e.setEquipAttr(str);
//       	 	String[] attrs = str.split("<br>");
//       	 	for(String attr:attrs) {
//       	 		System.out.println(attr.replaceAll("<[^>]+>", ""));
//       	 	}
       	}
        List<Selectable> spans = s.xpath("//span[@class='q2']").nodes();
        String effect = "";
        for(Selectable n:spans) {
        	System.out.println(n.replace("<[^>]+>", ""));
        	effect+=n.replace("<[^>]+>", "")+"\n";
        }
        e.setEquipEffect(effect);
        
        
		try {
			String result = HttpClientUtil.connect("http://db.nfuwow.com/60/ajax.php?item="+id+"&power");
			result=result.replaceAll("\\$WowheadPower.*?tooltip_cn: '", "").replaceAll("\\\\", "").replaceAll("<tr><td><div class=.*?;", "");
			e.setEquipTooltip(result);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String[] strs = imgUrl.split("/");
		String targetUrl = strs[strs.length-1];
		HttpClientUtil.downImage(imgUrl, targetUrl);
		e.setEquipImg(targetUrl);
        service.insert(e);
        System.out.println("**********************************************华丽的分割线*************************************************");
	}
	
	public static void main(String[] args) {
		String url = "http://image.nfuwow.com/static/db60/images/icons/large/inv_helmet_41.png";
		String[] strs = url.split("/");
		System.out.println(strs[strs.length-1]);
		
	}
	

}
