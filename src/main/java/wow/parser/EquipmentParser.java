package wow.parser;

import java.util.List;

import org.apache.http.client.HttpClient;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;
import wow.common.HttpClientUtil;
import wow.service.EquipmentService;

public class EquipmentParser implements PageProcessor {
    private Site site = Site.me().setRetryTimes(3).setSleepTime(1000);
    private static final String itemUrl = "http://db.nfuwow.com/60/";
    private EquipmentService service;
    public EquipmentParser(EquipmentService service) {
		this.service = service;
	}

	public Site getSite() {
        return site;
    }

    public void process(Page page) {
    	List<Selectable> nodes=page.getHtml().xpath("//div[@class='iconmedium']").nodes();
//    	Selectable s = nodes.get(0);
    	for(Selectable s:nodes) {
    		try {
    			String img = s.xpath("//ins/@style").toString().replaceAll("background-image: url", "").replaceAll("\'", "");
        		img=img.replaceAll("\\);", "").replaceAll("\\(", "");
        		String item = s.xpath("//a/@href").toString();
        		String id= item.replaceAll("\\?item=", "");
        		String url = itemUrl+item;
        		Spider.create(new EquipmentInfoParser(service,img,id))
        	      //从"https://github.com/code4craft"开始抓
        	      .addUrl(url)
        	      //开启5个线程抓取
        	      .thread(1)
        	      //启动爬虫
        	      .run();
    		}catch(Exception e) {
    			e.printStackTrace();
    			continue;
    		}
    	}
    }
  
}
