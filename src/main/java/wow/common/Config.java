package wow.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class Config  extends WebMvcConfigurerAdapter{
	 @Override
	    public void addViewControllers(ViewControllerRegistry registry) {
	        super.addViewControllers(registry);
	        //主页
	        registry.addViewController("/").setViewName("forward:/index.html");
	    } 
}
