package wow.common;

public class DamageItem {
	private String name;
	private String total;
	private String times;
	private String crit;
	private String hit;
	private String deflection;
	private String dodge;
	private String percent;
	private String missRate;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTimes() {
		return times;
	}
	public void setTimes(String times) {
		this.times = times;
	}
	public String getCrit() {
		return crit;
	}
	public void setCrit(String crit) {
		this.crit = crit;
	}
	public String getHit() {
		return hit;
	}
	public void setHit(String hit) {
		this.hit = hit;
	}
	public String getDeflection() {
		return deflection;
	}
	public void setDeflection(String deflection) {
		this.deflection = deflection;
	}
	public String getDodge() {
		return dodge;
	}
	public void setDodge(String dodge) {
		this.dodge = dodge;
	}
	public String getPercent() {
		return percent;
	}
	public void setPercent(String percent) {
		this.percent = percent;
	}
	public String getMissRate() {
		return missRate;
	}
	public void setMissRate(String missRate) {
		this.missRate = missRate;
	}
	
	
}
