package wow.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;


public class HttpClientUtil {
	public static String connect(String url) throws Exception {
		  CloseableHttpClient client = null;
          CloseableHttpResponse response = null;
        
            /**
             * 创建HttpClient对象
             */
            client = HttpClients.createDefault();
            /**
             * 创建URIBuilder
             */
            URIBuilder uriBuilder = new URIBuilder(url);
            /**
             * 设置参数
             */
            /**
             * 创建HttpGet
             */
            HttpGet httpGet = new HttpGet(uriBuilder.build());
            /**
             * 设置请求头部编码
             */
            httpGet.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"));
            /**
             * 设置返回编码
             */
            httpGet.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));
            /**
             * 请求服务
             */
            response = client.execute(httpGet);
            /**
             * 获取响应吗
             */
            int statusCode = response.getStatusLine().getStatusCode();

            if (200 == statusCode){
            	 HttpEntity entity = response.getEntity();
	             return  EntityUtils.toString(entity,"UTF-8");
            }else{
              System.out.println("请求失败！");
            }
	       
	        return null;
		
	}
	static String file = "D://pifu";//下载的目标路径
	public static void downImage(String imgurl, String fileName) {
			//判断目标文件夹是否存在
			File files = new File(file);
			if (!files.exists()) {
				files.mkdirs();
			}

			InputStream is;
			FileOutputStream out;
			try {
				File fileofImg = new File(file + "/" + fileName);
				if(fileofImg.exists()) {
					return;
				}
				URL url = new URL(imgurl);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				is = connection.getInputStream();
				// 创建文件
				
				out = new FileOutputStream(fileofImg);
				int i = 0;
				while ((i = is.read()) != -1) {
					out.write(i);
				}
				is.close();
				out.close();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	
	}
	
	public static void main(String[] args) {
		
		downImage("http://image.nfuwow.com/static/db60/images/icons/large/inv_helmet_41.png", "inv_helmet_41.png");
		
		
	}
}
