package wow.common;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.pagehelper.PageInfo;

@JsonIgnoreProperties(value = { "navigatepageNums", "prePage", "nextPage", "isFirstPage", "isLastPage",
		"hasPreviousPage", "hasNextPage", "navigatePages", "navigateFirstPage", "navigateLastPage", "firstPage",
		"lastPage" })
public class PageResponse extends PageInfo {
	private boolean success;
	private String message;
	private List<Map<String, Object>> footer;

	public PageResponse() {

	}

	public PageResponse(PageInfo pageInfo) {
		this.success = true;
		setPageNum(pageInfo.getPageNum());
		setPageSize(pageInfo.getPageSize());
		setSize(pageInfo.getSize());
		setStartRow(pageInfo.getStartRow());
		setEndRow(pageInfo.getEndRow());
		setTotal(pageInfo.getTotal());
		setPages(pageInfo.getPages());
		setList(pageInfo.getList());
		setPrePage(pageInfo.getPrePage());
		setNextPage(pageInfo.getNextPage());
		setIsFirstPage(pageInfo.isIsFirstPage());
		setIsLastPage(pageInfo.isIsLastPage());
		setHasPreviousPage(pageInfo.isHasPreviousPage());
		setHasNextPage(pageInfo.isHasNextPage());
		setNavigatePages(pageInfo.getNavigatePages());
		setNavigatepageNums(pageInfo.getNavigatepageNums());
		setNavigateFirstPage(pageInfo.getNavigateFirstPage());
		setNavigateLastPage(pageInfo.getNavigateLastPage());
	}

	@Override
	@JsonProperty("rows")
	public List getList() {
		return super.getList();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Map<String, Object>> getFooter() {
		return footer;
	}

	public void setFooter(List<Map<String, Object>> footer) {
		this.footer = footer;
	}

	public void addFooter(Map<String, Object> footermap) {
		footer.add(footermap);
	}
}
