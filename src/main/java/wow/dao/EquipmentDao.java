package wow.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import wow.entity.Equipment;

@Mapper
public interface EquipmentDao {
	public List<HashMap> findAll();
	public List<HashMap> getEquipList(Map searchParams);
	public void insert(Equipment entity);
	public Equipment findByCode(@Param("code")String code);
	public List<Equipment> findByCodes(@Param("codes")String[] codes);
	
}
