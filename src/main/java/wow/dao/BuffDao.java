package wow.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BuffDao {
	public List<HashMap> findAll();
}
