package wow.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;

import wow.common.HttpClientUtil;
import wow.common.PageResponse;
import wow.entity.Equipment;
import wow.entity.RequestData;
import wow.service.EquipmentService;
import wow.util.suit.RogueSuitUtil;
import wow.util.suit.WarriorSuitUtil;

@Controller
@RequestMapping(value = "/equip")
public class EquipmentController {
	@Autowired
	private EquipmentService service;
	
	@RequestMapping(value="getTooltip", method = RequestMethod.GET)
	@ResponseBody
	public String getTooltip(String code) {
		String result=null;
		try {
//			 result=HttpClientUtil.connect("http://db.nfuwow.com/60/ajax.php?item="+code+"&power");
//			 result=result.replaceAll("\\$WowheadPower.*?tooltip_cn: '", "").replaceAll("\\\\", "").replaceAll("<tr><td><div class=.*?;", "");
	 //			 result = "<table><tr><td><b class=\\\"q3\\\">少校的板层甲护肩<\\/b><span class=\\\"id-float-right\\\" style=\\\"float:right;\\\">ID: 23277<\\/span><br /><span class=\\\"q3\\\">Lieutenant Commander\\'s Lamellar Shoulders<\\/span><br /><span class=\\\"item-level\\\">物品等级 <\\/span><br />拾取绑定<br />唯一<table width=\\\"100%\\\"><tr><td>肩部<\\/td><th>板甲<\\/th><\\/tr><\\/table>552 盔甲<br />+14 耐力<br />+14 力量<br />+8 智力<br />耐久度 80 / 80<br />类别: <span class=\\\"c2\\\">圣骑士<\\/span><br />需要等级 60<br /><\\/td><\\/tr><\\/table><table><tr><td><span class=\\\"q2\\\">装备: <a href=\\\"?spell=14799\\\" class=\\\"q2\\\">提高所有法术和魔法效果所造成的伤害和治疗效果，最多20点。<\\/a><\\/span><br /><br /><span class=\\\"q\\\"><a href=\\\"?itemset=544\\\" class=\\\"q\\\">少校的壁垒<\\/a> (0/6)<\\/span><br /><div class=\\\"q0 indent\\\"><span><a href=\\\"?item=23272\\\">骑士队长的板层甲护胸<\\/a><\\/span><br /><span><a href=\\\"?item=23273\\\">骑士队长的板层甲护腿<\\/a><\\/span><br /><span><a href=\\\"?item=23274\\\">骑士中尉的板层甲护手<\\/a><\\/span><br /><span><a href=\\\"?item=23275\\\">骑士中尉的板层甲马靴<\\/a><\\/span><br /><span><a href=\\\"?item=23276\\\">少校的板层甲头盔<\\/a><\\/span><br /><span><a href=\\\"?item=23277\\\">少校的板层甲护肩<\\/a><\\/span><br /><\\/div><br /><span class=\\\"q0\\\"><span class=\\\"q0\\\"><span>(2) 套装: <a href=\\\"?spell=14047\\\">提高所有法术和魔法效果所造成的伤害和治疗效果，最多23点。<\\/a><\\/span><br /><span>(4) 套装: <a href=\\\"?spell=23302\\\">使你的制裁之锤的冷却时间减少10秒。<\\/a><\\/span><br /><span>(6) 套装: <a href=\\\"?spell=14467\\\">+20 耐力。<\\/a><\\/span><br /><\\/span><\\/span><\\/td><\\/tr><\\/table><tr><td><div class=\\'newsjktit\\'><a href=\\'http://db.nfuwow.com/60\\'>NFU数据库 db.nfuwow.com<\\/a><\\/div><\\/td><td><img class=\\'newsjkpic\\' src=\\'http://db.nfuwow.com/60/templates/wowhead/images/nfulogo.png\\'/><\\/td><\\/tr>".replace("%","%25");
			 Equipment e =service.findByCode(code);
			 if(null!=e) {
				 result=e.getEquipTooltip();
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="findByCode", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> findByCode(String code) {
		Map<String, Object> map =new HashMap<String, Object>();
		Equipment e =service.findByCode(code);
		map.put("success", true);
		map.put("data", e);
		return map;
//		return JSON.toJSONString(e);
//		return e;
	}
	
	@RequestMapping(value="getEquipList", method = RequestMethod.GET)
	@ResponseBody
	public PageResponse getEquipList(int page, int rows,String type,String position,String equipName) {
		PageResponse PageResponse = new PageResponse(service.getEquipList(page,rows,type,position,equipName));
		return PageResponse ;
	}
	
	@RequestMapping(value="suitTooltip",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> suitTooltip(@RequestBody RequestData requestData) {
		Map<String, Object> map =new HashMap<String, Object>();
		try {
			map.put("data", RogueSuitUtil.suitTooltip(requestData.getEquipments()));
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		return map;
	}
	@RequestMapping(value="warriorSuitTooltip",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> warriorSuitTooltip(@RequestBody RequestData requestData) {
		Map<String, Object> map =new HashMap<String, Object>();
		try {
			map.put("data", WarriorSuitUtil.suitTooltip(requestData.getEquipments()));
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		return map;
	}
}
