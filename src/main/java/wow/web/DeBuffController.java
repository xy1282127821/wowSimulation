package wow.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import wow.service.DeBuffService;

@Controller
@RequestMapping(value = "/debuff")
public class DeBuffController {
	@Autowired
	private DeBuffService service;
	
	@RequestMapping(value="findAll", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> findAll() {
		Map<String, Object> map =new HashMap<String, Object>();
		try {
			map.put("rows", service.findAll());
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		return map;
	}

}
