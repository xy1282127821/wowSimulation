package wow.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import wow.common.DamageItem;
import wow.entity.RequestData;
import wow.service.CalculationService;
import wow.util.rouge.RougeRole;
import wow.util.warrior.WarriorRole;

@Controller
@RequestMapping(value = "/index")
public class IndexController {
	@Autowired
	private CalculationService service;
	
	@RequestMapping(value="getRogueAttr", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getRogueAttr(@RequestBody RequestData requestData,HttpServletRequest request){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			//String race,String[] equipments,String[] buffs,
			RougeRole r = service.getRogueAttr(requestData);
			request.getSession().setAttribute("role", r);
			result.put("data",r);
			result.put("success", true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("success", false);
		}
		return result;
	}
	@RequestMapping(value="getWarriorAttr", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getWarriorAttr(@RequestBody RequestData requestData,HttpServletRequest request){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			//String race,String[] equipments,String[] buffs,
			WarriorRole r = service.getWarriorAttr(requestData);
			request.getSession().setAttribute("warriorRole", r);
			result.put("data",r);
			result.put("success", true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("success", false);
		}
		return result;
	}
	
	@RequestMapping(value="getWarriorSimulation")
	@ResponseBody
	public Map<String, Object> getWarriorSimulation(int ac,HttpServletRequest request){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			//String race,String[] equipments,String[] buffs,
			WarriorRole r = (WarriorRole) request.getSession().getAttribute("warriorRole");
			List<DamageItem>  list =service.getWarriorSimulation(r,ac);
			result.put("rows",list);
			result.put("success", true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("success", false);
		}
		return result;
	}
	
	
	@RequestMapping(value="getSimulation")
	@ResponseBody
	public Map<String, Object> getSimulation(int ac,HttpServletRequest request){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			//String race,String[] equipments,String[] buffs,
			RougeRole r = (RougeRole) request.getSession().getAttribute("role");
			List<DamageItem>  list =service.getSimulation(r,ac);
			result.put("rows",list);
			result.put("success", true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("success", false);
		}
		return result;
	}

}
