package wow.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import wow.service.BuffService;

@Controller
@RequestMapping(value = "/buff")
public class BuffController {
	@Autowired
	private BuffService service;
	
	@RequestMapping(value="findAll", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> findAll() {
		Map<String, Object> map =new HashMap<String, Object>();
		try {
			map.put("rows", service.findAll());
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		return map;
	}
}
