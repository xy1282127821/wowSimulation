package wow.util.warrior;

public class NightElfWarrior extends WarriorRole {
	
	public NightElfWarrior() {
		super();
		super.setAp(160);
		super.setStrength(117); //力量
		super.setNimble(85); //敏捷
		super.setIntelligence(30); //智力
		super.setMental(45);//精神
		super.setEndurance(109); //耐力
		super.setMainHandSkill(300);
		super.setOffHandSkill(300);
	}
	
}
