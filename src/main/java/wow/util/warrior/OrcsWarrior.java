package wow.util.warrior;

import wow.entity.Equipment;

public class OrcsWarrior extends WarriorRole{
	public  OrcsWarrior() {
		super();
		super.setAp(160);
		super.setStrength(120); //力量
		super.setNimble(80); //敏捷
		super.setIntelligence(30); //智力
		super.setMental(47);//精神
		super.setEndurance(110); //耐力
		super.setMainHandSkill(300);
		super.setOffHandSkill(300);
	}
	
	public void getAttr(Equipment mainHandWeapon,Equipment offHandWeapon){
		super.getAttr(mainHandWeapon, offHandWeapon);
		if(mainHandWeapon.getEquipType().equals("斧")||mainHandWeapon.getEquipType().equals("斧")) {
			super.setMainHandSkill(super.getMainHandSkill()+5);
		}
		if(offHandWeapon!=null&&offHandWeapon.getEquipType().equals("斧")||mainHandWeapon.getEquipType().equals("斧")) {
			super.setOffHandSkill(super.getOffHandSkill()+5);
		}
	}
}
