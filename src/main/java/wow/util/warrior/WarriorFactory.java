package wow.util.warrior;


public class WarriorFactory {
	public static WarriorRole getInstence(String race) throws Exception{
		WarriorRole r = (WarriorRole) Class.forName("wow.util.warrior."+race).newInstance();
		return r;
	}
}
