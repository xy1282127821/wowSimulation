package wow.util.warrior;

import java.text.DecimalFormat;

import wow.entity.Equipment;
import wow.util.Role;

public abstract class WarriorRole extends Role{
	
	public void getAttr(Equipment mainHandWeapon,Equipment offHandWeapon){
//		System.out.println("ap："+ap);
//		System.out.println("力量："+strength);
//		System.out.println("敏捷："+nimble);
//		System.out.println("暴击率"+crit);
		this.ap +=strength*2;
//		DecimalFormat decimalFormat=new DecimalFormat("#.##");
		this.crit += (float) (nimble/20.0f);
//		System.out.println("最终ap:"+ap);
//		System.out.println("最终crit:"+decimalFormat.format(crit+0.001));
//		System.out.println("hitRate:"+hitRate);
//		System.out.println("武器技能："+mainHandSkill);
		
		mainHandWeaponType = mainHandWeapon.getEquipType();
		
		
	
		
		DecimalFormat decimalFormat2=new DecimalFormat("#");
		this.mainHandSpeed=Float.parseFloat(mainHandWeapon.getEquipSpeed().replaceAll("速度", "").trim());
		mainHandWeaponDamages = mainHandWeapon.getEquipDamage().replace("伤害", "").trim();
		String[] mainHandWeaponDamages=mainHandWeapon.getEquipDamage().replace("伤害", "").trim().split("-");
		
	
		float mainHandLowDamage= Float.parseFloat(mainHandWeaponDamages[0])+ap*mainHandSpeed/14;
		float mainHandHighDamage= Float.parseFloat(mainHandWeaponDamages[1])+ap*mainHandSpeed/14;
		if(special.contains("m+8")) {
			mainHandLowDamage+=8;
			mainHandHighDamage+=8;
		}
		
		this.mainHandDamage=decimalFormat2.format(mainHandLowDamage)+"-"+decimalFormat2.format(mainHandHighDamage);
		this.mainHandDps = (mainHandLowDamage+mainHandHighDamage)/2/mainHandSpeed;
		
//		System.out.println("主手速度:"+mainHandSpeed);
//		System.out.println("主手伤害:"+mainHandDamage);
//		System.out.println("主手秒伤:"+mainHandDps);
//		
		if(offHandWeapon!=null) {
			offHandWeaponType = offHandWeapon.getEquipType();
			this.offHandSpeed=Float.parseFloat(offHandWeapon.getEquipSpeed().replaceAll("速度", "").trim());
			String[] offHandWeaponDamages=offHandWeapon.getEquipDamage().replace("伤害", "").trim().split("-");
			double offHandLowDamage= (Float.parseFloat(offHandWeaponDamages[0])+ap*offHandSpeed/14)/2*1.25;
			double offHandHighDamage= (Float.parseFloat(offHandWeaponDamages[1])+ap*offHandSpeed/14)/2*1.25;
			
			if(special.contains("o+8")) {
				offHandLowDamage += 8/2*1.25;
				offHandHighDamage +=  8/2*1.25;
			}
			
			this.offHandDamage=decimalFormat2.format(offHandLowDamage)+"-"+decimalFormat2.format(offHandHighDamage);
			this.offHandDps =(float) ((offHandLowDamage+offHandHighDamage)/2/offHandSpeed);
			offHandCrit +=crit;

		}
		
		mainHandCrit +=crit;
//		System.out.println("副手速度:"+offHandSpeed);
//		System.out.println("副手伤害:"+offHandDamage);
//		System.out.println("副手秒伤:"+offHandDps);
	}
	
}
