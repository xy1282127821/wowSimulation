package wow.util.suit;

import java.util.ArrayList;
import java.util.List;

import wow.util.Role;
import wow.util.rouge.RougeRole;
import wow.util.suit.suitType.BlackDragonscaleCaculate;
import wow.util.suit.suitType.CadaverousCaculate;
import wow.util.suit.suitType.DalRendsDoubleSwordCaculate;
import wow.util.suit.suitType.DevilsaurCaculate;
import wow.util.suit.suitType.RogueT0Caculate;
import wow.util.warrior.WarriorRole;

public class WarriorSuitUtil {
	public static final  String[] DalRendsDoubleSword = {"12940","12939"}; 
	public static final String[] Devilsaur= {"15063","15062"};
	public static final String[] Cadaverous= {"14637","14636","14638","14640","14641"};
	public static final String[] BlackDragonscale = {"15051","15050","15052","16984"};
	
	public static void suitEffect(Role role,String[] codes) {
		DalRendsDoubleSwordCaculate.getInstance().caculate(role, isExistArr(codes, DalRendsDoubleSword));
		DevilsaurCaculate.getInstance().caculate(role, isExistArr(codes, Devilsaur));
		CadaverousCaculate.getInstance().caculate(role, isExistArr(codes, Cadaverous));
		BlackDragonscaleCaculate.getInstance().caculate(role, isExistArr(codes, BlackDragonscale));
	}
	public static List<SuitTooltip> suitTooltip(String[] codes) {
		List<SuitTooltip> result = new ArrayList<SuitTooltip>();
		result.addAll(DalRendsDoubleSwordCaculate.getInstance().toolTip(isExistArr(codes, DalRendsDoubleSword)));
		result.addAll(DevilsaurCaculate.getInstance().toolTip(isExistArr(codes, Devilsaur)));
		result.addAll(CadaverousCaculate.getInstance().toolTip(isExistArr(codes, Cadaverous)));
		result.addAll(BlackDragonscaleCaculate.getInstance().toolTip(isExistArr(codes, BlackDragonscale)));
		return result;
	}
	private static int isExistArr(String[] codes,String[] suitCodes) {
		int i = 0;
		for(String suitCode:suitCodes) {
			for(String code:codes) {
				if(suitCode.equals(code)) {
					i++;
				}
			}
		}
		return i;
	}
}
