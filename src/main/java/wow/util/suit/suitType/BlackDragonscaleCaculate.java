package wow.util.suit.suitType;

import java.util.ArrayList;
import java.util.List;

import wow.util.Role;
import wow.util.suit.SuitCaculate;
import wow.util.suit.SuitTooltip;

public class BlackDragonscaleCaculate implements SuitCaculate{
	private static BlackDragonscaleCaculate instance;
	
	private BlackDragonscaleCaculate() {
		
	}
	public static BlackDragonscaleCaculate getInstance() {
		if(instance==null) {
			instance = new BlackDragonscaleCaculate();
		}
		return instance;
	}
	
	
	
	@Override
	public void caculate(Role race, int num) {
		// TODO Auto-generated method stub
		if(num>=2) {
			race.hitRate+=1;
		}
		if(num==3) {
			race.crit+=2;
		}

	}

	@Override
	public List<SuitTooltip> toolTip(int num) {
		List<SuitTooltip> result = new ArrayList<SuitTooltip>();
		if(num>=2) {
			result.add(new SuitTooltip("黑色龙鳞", "2/4", "使你命中目标的几率提高1%。"));
		}
		if(num>=3) {
			result.add(new SuitTooltip("黑色龙鳞", "3/4", "使你致命一击的几率提高2%。"));
		}
		if(num>=4) {
			result.add(new SuitTooltip("黑色龙鳞", "4/4", "+10火焰抗性。"));
		}
		return result;
	}

}
