package wow.util.suit.suitType;

import java.util.ArrayList;
import java.util.List;

import wow.util.Role;
import wow.util.suit.SuitCaculate;
import wow.util.suit.SuitTooltip;

public class DalRendsDoubleSwordCaculate implements SuitCaculate{
	private static DalRendsDoubleSwordCaculate instance;
	
	private DalRendsDoubleSwordCaculate() {
		
	}
	public static DalRendsDoubleSwordCaculate getInstance() {
		if(instance==null) {
			instance = new DalRendsDoubleSwordCaculate();
		}
		return instance;
	}
	

	@Override
	public void caculate(Role race, int num) {
		if(num==2) {
			race.setAp(race.getAp()+50);
		}
	}
	@Override
	public List<SuitTooltip> toolTip(int num) {
		List<SuitTooltip> result = new ArrayList<SuitTooltip>();
		if(num>=2) {
			result.add(new SuitTooltip("雷德双刀", "2/2", "+50AP"));
		}
		return result;
	}

}
