package wow.util.suit.suitType;

import java.util.ArrayList;
import java.util.List;

import wow.util.Role;
import wow.util.suit.SuitCaculate;
import wow.util.suit.SuitTooltip;

public class RogueT0Caculate implements SuitCaculate {
	private static RogueT0Caculate instance;
	private RogueT0Caculate() {
		
	}
	public static RogueT0Caculate getInstance() {
		if(instance==null) {
			instance = new RogueT0Caculate();
		}
		return instance;
	}
	

	@Override
	public void caculate(Role race, int num) {
		if(num>=4) {
			race.setAp(race.getAp()+40);
		}
		if(num>=6) {
			race.getSpecial().add("RogueT0*6Eeffect");
		}
	}
	@Override
	public List<SuitTooltip> toolTip(int num) {
		List<SuitTooltip> result = new ArrayList<SuitTooltip>();
		if(num>=2) {
			result.add(new SuitTooltip("迅影", "2/8", "+200点护甲值。"));
		}
		if(num>=4) {
			result.add(new SuitTooltip("迅影", "4/8", "攻击强度提高40点。"));
		}
		if(num>=6) {
			result.add(new SuitTooltip("迅影", "6/8", "有一定几率在近战攻击时为你恢复35点能量值。"));
		}
		if(num>=8) {
			result.add(new SuitTooltip("迅影", "8/8", "+8 所有魔法抗性。"));
		}
		return result;
	}
}
