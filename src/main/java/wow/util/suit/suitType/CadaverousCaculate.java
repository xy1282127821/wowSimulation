package wow.util.suit.suitType;

import java.util.ArrayList;
import java.util.List;

import wow.util.Role;
import wow.util.suit.SuitCaculate;
import wow.util.suit.SuitTooltip;

public class CadaverousCaculate  implements SuitCaculate{
	private static CadaverousCaculate instance;
	
	private CadaverousCaculate() {
		
	}
	public static CadaverousCaculate getInstance() {
		if(instance==null) {
			instance = new CadaverousCaculate();
		}
		return instance;
	}
	

	@Override
	public void caculate(Role race, int num) {
		if(num>=3) {
			race.setAp(race.getAp()+10);
		}
		if(num==5) {
			race.setHitRate(race.getHitRate()+2);
		}
	}
	@Override
	public List<SuitTooltip> toolTip(int num) {
		List<SuitTooltip> result = new ArrayList<SuitTooltip>();
		if(num>=2) {
			result.add(new SuitTooltip("苍白", "2/5", "防御技能提高3点。"));
		}
		if(num>=3) {
			result.add(new SuitTooltip("苍白", "3/5", "+10 攻击强度。"));
		}
		if(num>=4) {
			result.add(new SuitTooltip("苍白", "4/5", "+15 所有魔法抗性。"));
		}
		if(num>=5) {
			result.add(new SuitTooltip("苍白", "5/5", "使你击中目标的几率提高2%。"));
		}
		return result;
	}
}
