package wow.util.suit.suitType;

import java.util.ArrayList;
import java.util.List;

import wow.util.Role;
import wow.util.suit.SuitCaculate;
import wow.util.suit.SuitTooltip;

public class DevilsaurCaculate implements SuitCaculate {
	private static DevilsaurCaculate instance;
	
	private DevilsaurCaculate() {
		
	}
	public static DevilsaurCaculate getInstance() {
		if(instance==null) {
			instance = new DevilsaurCaculate();
		}
		return instance;
	}
	

	@Override
	public void caculate(Role race, int num) {
		if(num==2) {
			race.setHitRate(race.getHitRate()+2);
		}
	}
	@Override
	public List<SuitTooltip> toolTip(int num) {
		List<SuitTooltip> result = new ArrayList<SuitTooltip>();
		if(num>=2) {
			result.add(new SuitTooltip("魔暴龙套装", "2/2", "命中目标的几率提高2%"));
		}
		return result;
	}
}
