package wow.util.suit;

import java.util.List;

import wow.util.Role;


public interface SuitCaculate {
	public void caculate(Role race,int num);
	public List<SuitTooltip> toolTip(int num);
}
