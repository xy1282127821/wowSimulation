package wow.util.suit;

public class SuitTooltip {
	private String name;
	private String num;
	private String tooltip;
	
	public SuitTooltip(String name, String num, String tooltip) {
		super();
		this.name = name;
		this.num = num;
		this.tooltip = tooltip;
	}
	
	public SuitTooltip() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getTooltip() {
		return tooltip;
	}
	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}
	
	
}
