package wow.util.suit;

import java.util.ArrayList;
import java.util.List;

import wow.util.rouge.RougeRole;
import wow.util.suit.suitType.CadaverousCaculate;
import wow.util.suit.suitType.DalRendsDoubleSwordCaculate;
import wow.util.suit.suitType.DevilsaurCaculate;
import wow.util.suit.suitType.RogueT0Caculate;

public class RogueSuitUtil {
	public static final  String[] DalRendsDoubleSword = {"12940","12939"}; 
	public static final String[] Devilsaur= {"15063","15062"};
	public static final String[] RogueT0= {"16707","16708","16721","16710","16711","16712","16713","16709"};
	public static final String[] Cadaverous= {"14637","14636","14638","14640","14641"};
	public static void suitEffect(RougeRole race,String[] codes) {
		DalRendsDoubleSwordCaculate.getInstance().caculate(race, isExistArr(codes, DalRendsDoubleSword));
		DevilsaurCaculate.getInstance().caculate(race, isExistArr(codes, Devilsaur));
		CadaverousCaculate.getInstance().caculate(race, isExistArr(codes, Cadaverous));
		RogueT0Caculate.getInstance().caculate(race, isExistArr(codes, RogueT0));
	}
	public static List<SuitTooltip> suitTooltip(String[] codes) {
		List<SuitTooltip> result = new ArrayList<SuitTooltip>();
		result.addAll(DalRendsDoubleSwordCaculate.getInstance().toolTip(isExistArr(codes, DalRendsDoubleSword)));
		result.addAll(DevilsaurCaculate.getInstance().toolTip(isExistArr(codes, Devilsaur)));
		result.addAll(CadaverousCaculate.getInstance().toolTip(isExistArr(codes, Cadaverous)));
		result.addAll(RogueT0Caculate.getInstance().toolTip(isExistArr(codes, RogueT0)));
		return result;
	}
	private static int isExistArr(String[] codes,String[] suitCodes) {
		int i = 0;
		for(String suitCode:suitCodes) {
			for(String code:codes) {
				if(suitCode.equals(code)) {
					i++;
				}
			}
		}
		return i;
	}
}
