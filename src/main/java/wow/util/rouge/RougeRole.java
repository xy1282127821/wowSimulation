package wow.util.rouge;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import wow.entity.Equipment;
import wow.util.Role;

public abstract class RougeRole  extends Role{
	public Equipment mainHandWeapon;
	public Equipment offHandWeapon;
	
	public void getAttr(Equipment mainHandWeapon,Equipment offHandWeapon){
		
//		System.out.println("ap："+ap);
//		System.out.println("力量："+strength);
//		System.out.println("敏捷："+nimble);
//		System.out.println("暴击率"+crit);
		super.ap +=strength+nimble;
		DecimalFormat decimalFormat=new DecimalFormat("#.##");
		super.crit += (float) (nimble/29.0);
//		System.out.println("最终ap:"+ap);
//		System.out.println("最终crit:"+decimalFormat.format(crit+0.001));
//		System.out.println("hitRate:"+hitRate);
//		System.out.println("武器技能："+mainHandSkill);
		
		mainHandWeaponType = mainHandWeapon.getEquipType();
		offHandWeaponType = offHandWeapon.getEquipType();
		
		if(mainHandWeaponType.equals("匕首")) {
			super.crit +=5;
		}
		super.mainHandSkill +=5;
		super.offHandSkill +=5;
		super.hitRate+=5;
		
		
		DecimalFormat decimalFormat2=new DecimalFormat("#");
		super.mainHandSpeed=Float.parseFloat(mainHandWeapon.getEquipSpeed().replaceAll("速度", "").trim());
		mainHandWeaponDamages = mainHandWeapon.getEquipDamage().replace("伤害", "").trim();
		String[] mainHandWeaponDamages=mainHandWeapon.getEquipDamage().replace("伤害", "").trim().split("-");
		
		float mainHandLowDamage= Float.parseFloat(mainHandWeaponDamages[0])+ap*mainHandSpeed/14;
		float mainHandHighDamage= Float.parseFloat(mainHandWeaponDamages[1])+ap*mainHandSpeed/14;
		
		if(special.contains("m+8")) {
			mainHandLowDamage+=8;
			mainHandHighDamage+=8;
		}
		
		super.mainHandDamage=decimalFormat2.format(mainHandLowDamage)+"-"+decimalFormat2.format(mainHandHighDamage);
		super.mainHandDps = (mainHandLowDamage+mainHandHighDamage)/2/mainHandSpeed;
		
//		System.out.println("主手速度:"+mainHandSpeed);
//		System.out.println("主手伤害:"+mainHandDamage);
//		System.out.println("主手秒伤:"+mainHandDps);
//		
		super.offHandSpeed=Float.parseFloat(offHandWeapon.getEquipSpeed().replaceAll("速度", "").trim());
		String[] offHandWeaponDamages=offHandWeapon.getEquipDamage().replace("伤害", "").trim().split("-");
		double offHandLowDamage= (Float.parseFloat(offHandWeaponDamages[0])+ap*offHandSpeed/14)/2*1.5;
		double offHandHighDamage= (Float.parseFloat(offHandWeaponDamages[1])+ap*offHandSpeed/14)/2*1.5;
		
		if(special.contains("o+8")) {
			offHandLowDamage += 8/2*1.5;
			offHandHighDamage +=  8/2*1.5;
		}
		
		super.offHandDamage=decimalFormat2.format(offHandLowDamage)+"-"+decimalFormat2.format(offHandHighDamage);
		super.offHandDps =(float) ((offHandLowDamage+offHandHighDamage)/2/offHandSpeed);
		
		
		mainHandCrit +=crit;
		offHandCrit +=crit;
		
		this.mainHandWeapon= mainHandWeapon;
		this.offHandWeapon = offHandWeapon;
		
//		System.out.println("副手速度:"+offHandSpeed);
//		System.out.println("副手伤害:"+offHandDamage);
//		System.out.println("副手秒伤:"+offHandDps);
	}
	
}
