package wow.util.rouge;

public class Undead extends RougeRole {
	
	public Undead() {
		super();
		super.setAp(100);
		super.setStrength(79); //力量
		super.setNimble(128); //敏捷
		super.setIntelligence(33); //智力
		super.setMental(55);//精神
		super.setEndurance(78); //耐力
		super.setMainHandSkill(300);
		super.setOffHandSkill(300);
	}
	
}
