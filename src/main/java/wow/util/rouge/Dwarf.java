package wow.util.rouge;

public class Dwarf extends RougeRole {

	public Dwarf() {
		super();
		super.setAp(100);
		super.setStrength(82); //力量
		super.setNimble(126); //敏捷
		super.setIntelligence(34); //智力
		super.setMental(49);//精神
		super.setEndurance(78); //耐力
		super.setMainHandSkill(300);
		super.setOffHandSkill(300);
	}
	
	
	
}
