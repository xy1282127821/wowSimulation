package wow.util.rouge;

public class RaceFactory {
	public static RougeRole getInstence(String race) throws Exception{
		RougeRole r = (RougeRole) Class.forName("wow.util.rouge."+race).newInstance();
		return r;
	}
}
