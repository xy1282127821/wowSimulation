package wow.util.rouge;

import wow.entity.Equipment;

public class Human extends RougeRole{
	public Human() {
		super();
		super.setAp(100);
		super.setStrength(80); //力量
		super.setNimble(130); //敏捷
		super.setIntelligence(35); //智力
		super.setMental(52);//精神
		super.setEndurance(75); //耐力
		super.setMainHandSkill(300);
		super.setOffHandSkill(300);
	}
	
	public void getAttr(Equipment mainHandWeapon,Equipment offHandWeapon){
		super.getAttr(mainHandWeapon, offHandWeapon);
		if(mainHandWeapon.getEquipType().equals("剑")) {
			super.setMainHandSkill(super.getMainHandSkill()+5);
		}
		if(offHandWeapon.getEquipType().equals("剑")) {
			super.setOffHandSkill(super.getOffHandSkill()+5);
		}
	}
}
