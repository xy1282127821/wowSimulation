package wow.util;

import java.util.ArrayList;
import java.util.List;

public abstract class Role {
	public int strength; //力量
	public int nimble;   //敏捷
	public int intelligence; //智力
	public int mental; //精神
	public int endurance; //耐力
	public int ap; //ap
	public float crit=5f; //暴击率
	public float hitRate=0f; // 命中率
	public float mainHandSpeed; //主手速度
	public float offHandSpeed; //副手速度
	public int mainHandSkill; //主手武器技能 
	public int offHandSkill; //副手武器技能
	public String mainHandDamage;
	public String offHandDamage;
	public float mainHandDps;
	public float offHandDps;
	public float mainHandHitRate;
	public float offHandHitRate;
	public String mainHandWeaponDamages;
	public String mainHandWeaponType;
	public String offHandWeaponType;
	public float mainHandCrit=0;
	public float offHandCrit=0;
	public List<String> special = new ArrayList<String>(); 
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	public int getNimble() {
		return nimble;
	}
	public void setNimble(int nimble) {
		this.nimble = nimble;
	}
	public int getIntelligence() {
		return intelligence;
	}
	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}
	public int getMental() {
		return mental;
	}
	public void setMental(int mental) {
		this.mental = mental;
	}
	public int getEndurance() {
		return endurance;
	}
	public void setEndurance(int endurance) {
		this.endurance = endurance;
	}
	public int getAp() {
		return ap;
	}
	public void setAp(int ap) {
		this.ap = ap;
	}
	public float getCrit() {
		return crit;
	}
	public void setCrit(float crit) {
		this.crit = crit;
	}
	public float getHitRate() {
		return hitRate;
	}
	public void setHitRate(float hitRate) {
		this.hitRate = hitRate;
	}
	public float getMainHandSpeed() {
		return mainHandSpeed;
	}
	public void setMainHandSpeed(float mainHandSpeed) {
		this.mainHandSpeed = mainHandSpeed;
	}
	public float getOffHandSpeed() {
		return offHandSpeed;
	}
	public void setOffHandSpeed(float offHandSpeed) {
		this.offHandSpeed = offHandSpeed;
	}
	public int getMainHandSkill() {
		return mainHandSkill;
	}
	public void setMainHandSkill(int mainHandSkill) {
		this.mainHandSkill = mainHandSkill;
	}
	public int getOffHandSkill() {
		return offHandSkill;
	}
	public void setOffHandSkill(int offHandSkill) {
		this.offHandSkill = offHandSkill;
	}
	
	
	
	public String getMainHandDamage() {
		return mainHandDamage;
	}
	public void setMainHandDamage(String mainHandDamage) {
		this.mainHandDamage = mainHandDamage;
	}
	public String getOffHandDamage() {
		return offHandDamage;
	}
	public void setOffHandDamage(String offHandDamage) {
		this.offHandDamage = offHandDamage;
	}
	public float getMainHandDps() {
		return mainHandDps;
	}
	public void setMainHandDps(float mainHandDps) {
		this.mainHandDps = mainHandDps;
	}
	public float getOffHandDps() {
		return offHandDps;
	}
	public void setOffHandDps(float offHandDps) {
		this.offHandDps = offHandDps;
	}
	
	
	public float getMainHandHitRate() {
		return mainHandHitRate;
	}
	public void setMainHandHitRate(float mainHandHitRate) {
		this.mainHandHitRate = mainHandHitRate;
	}
	public float getOffHandHitRate() {
		return offHandHitRate;
	}
	public void setOffHandHitRate(float offHandHitRate) {
		this.offHandHitRate = offHandHitRate;
	}
	
	public String getMainHandWeaponDamages() {
		return mainHandWeaponDamages;
	}
	public void setMainHandWeaponDamages(String mainHandWeaponDamages) {
		this.mainHandWeaponDamages = mainHandWeaponDamages;
	}
	
	public List<String> getSpecial() {
		return special;
	}
	public void setSpecial(List<String> special) {
		this.special = special;
	}
	
	
	public String getMainHandWeaponType() {
		return mainHandWeaponType;
	}
	public void setMainHandWeaponType(String mainHandWeaponType) {
		this.mainHandWeaponType = mainHandWeaponType;
	}
	public String getOffHandWeaponType() {
		return offHandWeaponType;
	}
	public void setOffHandWeaponType(String offHandWeaponType) {
		this.offHandWeaponType = offHandWeaponType;
	}
	public float getMainHandCrit() {
		return mainHandCrit;
	}
	public void setMainHandCrit(float mainHandCrit) {
		this.mainHandCrit = mainHandCrit;
	}
	public float getOffHandCrit() {
		return offHandCrit;
	}
	public void setOffHandCrit(float offHandCrit) {
		this.offHandCrit = offHandCrit;
	}
	
}
