package wow;

import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import us.codecraft.webmagic.Spider;
import wow.entity.Equipment;
import wow.parser.EquipmentParser;
import wow.service.EquipmentService;
@RunWith(SpringRunner.class) // 等价于使用 @RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ApplicationTest {
	@Autowired
	public EquipmentService service;
	
	@Test
	public void applicationTest() {
		for(int i=1;i<=17;i++) {
			try {
				Spider.create(new EquipmentParser(service))
			      //从"https://github.com/code4craft"开始抓
			      .addUrl("http://db.nfuwow.com/60/?items=4.3&page="+i)
			      //开启5个线程抓取
			      .thread(1)
			      //启动爬虫
			      .run();
			}catch(Exception e) {
				e.printStackTrace();
				Equipment entity = new Equipment();
				entity.setEquipType("出错");
				entity.setEquipPostion(i+"");
				service.insert(entity);
				continue;
			}
		}
//		 List<HashMap> list =service.findAll(); 
//		 for(HashMap map : list) {
//			 System.out.println(map.toString());
//		 }
//		 System.out.println("312312");
	}
}
